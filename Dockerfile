FROM node:18-alpine

ADD . /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN yarn install
EXPOSE 3000
COPY . ./
CMD yarn dev
