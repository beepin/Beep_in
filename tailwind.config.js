/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'karla': ['"karla"'],
        'inter': ['"inter"'],
      },
      backgroundColor: {
        'transBlue' : 'rgba(76, 175, 80, 0.05)'
      },
      animation: {
        'spin-slow': 'spin 5s linear infinite',
        'ping-slow': 'ping 5s cubic-bezier(1, 1, 0.2, 1) infinite',

      },
       keyframes: {
         spin: {
           to:{
             transform: 'rotate(-360deg)'
           }
         },
         wave: {
          '0%': { transform: 'opacity()' },
          '10%': { transform: 'rotate(14deg)' },
          '20%': { transform: 'rotate(-8deg)' },
          '30%': { transform: 'rotate(14deg)' },
          '40%': { transform: 'rotate(-4deg)' },
          '50%': { transform: 'rotate(10.0deg)' },
          '60%': { transform: 'rotate(0.0deg)' },
          '100%': { transform: 'rotate(0.0deg)' },
        },
        }
    },
    screens: {
      
      'sm':{min:"100px",max:"480px"},
      'md':{min:"481px",max:"768px"},
      'lg':{min:"769px",max:"1024px"},
      'xl':{min:"1025px",max:"1200px"},
      '2xl':{min:"1201px",max:"1536px"},
      '3xl':{min:"1537px",max:"2000px"}
    }
  },
  plugins: [
    require('tailwind-scrollbar')
  ],
}// import useDownloader from "react-use-downloader";
