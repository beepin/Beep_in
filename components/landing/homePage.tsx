import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Telephone, EnvelopeOpen } from 'react-bootstrap-icons';
function HomePage() {
  return (
    <div className="w-full ">
      <nav className="flex justify-between sticky inset-x-0 top-0 bg-white items-center h-20 left-0 w-full mt-8 ">
        <img
          src="icons/logo.svg"
          alt="logo"
          className="3xl:ml-28 2xl:ml-28 xl:ml-28 lg:ml-14 md:ml-0 sm:ml-0"
        />
        <div className="3xl:flex 2xl:flex xl:flex lg:flex md:flex sm:absolute sm:right-2 float-right">
          <Link href="auth/login">
            <button className="h-12 bg-white text-[#6C63FF] shadow-md shadow-slate-700 w-28 rounded-lg 3xl:mr-6 2xl:mr-6 xl:mr-6 lg:mr-6 md:mr-4 sm:mr-4 font-karla hover:text-green-800">
              Login
            </button>
          </Link>
          <Link href="auth/signup">
            <button className="bg-gradient-to-l from-green-700 to-[#6C63FF] text-white rounded-lg h-12 w-28 3xl:mr-28 2xl:mr-28 xl:mr-28 lg:mr-12 md:mr-0 sm:mr-0 font-karla hover:text-green-800">
              Sign up
            </button>
          </Link>
        </div>
      </nav>
      <div className="3xl:flex 2xl:flex xl:flex lg:flex md:block sm:block mt-20">
        <div className="3xl:w-[50vw] 2xl:w-[50vw] xl:w-[50vw] lg:w-[50vw]  md:w-full sm:w-full">
          <div className="3xl:pl-28 2xl:pl-28 xl:pl-28 lg:pl-14 md:pl-0 sm:pl-0 3xl:w-[30vw] 2xl:w-[30vw] xl:w-[30vw] lg:w-[35vw] pt-[8%]">
            <h1 className="text-[#6C63FF] text-4xl font-karla ">
              Easily engage your customers anywhere.
            </h1>
            <p className="pt-8 font-inter text-lg text-black text-opacity-50">
            Enhance customer loyalty and retention by guaranteeing the delivery of bulk, group or single message.
            </p>
            <button className="bg-gradient-to-l from-green-700 to-[#6C63FF] text-white rounded-lg h-12 w-40 mt-10 font-karla  text-xl">
              Get Started
            </button>
          </div>
        </div>
        <div className="">
          <div className='md:flex sm:block'>
          <div className="h-88 3xl:w-80 2xl:w-80 xl:w-80 lg:w-64 md:w-72 sm:w-96 bg-[#D9D9D9] rounded-lg 3xl:mt-4 2xl:mt-4 xl:mt-4 lg:mt-4 md:mt-12 sm:mt-12">
            <img
              src="/images/home.png"
              alt="home"
              className="3xl:h-80 2xl:h-80 xl:h-80 lg:h-80 md:h-72 sm:h-96"
            />
          </div>
          <div className="h-88 3xl:w-80 2xl:w-80 xl:w-80 lg:w-64 md:w-72 sm:w-96 bg-[#D9D9D9] rounded-xl 3xl:absolute 2xl:absolute xl:absolute lg:absolute sm:mt-12 md:mt-12 3xl:top-[40%] 2xl:top-[40%] xl:top-[40%] lg:top-[40%]  3xl:ml-[10%] 2xl:ml-[10%] xl:ml-[10%] lg:ml-[10%] md:ml-10">
            <img
              src="/images/home2.png"
              alt="home"
              className="3xl:h-80 3xl:w-80 2xl:w-80 xl:w-80 lg:w-64 md:w-72 sm:w-96 rounded-xl 2xl:h-80 xl:h-80 lg:h-80 md:h-72 sm:h-96"
            />
          </div>
          </div>
          <div className="flex  md:hidden sm:hidden">
            <div className="flex">
              <div className="bg-[#6c63ff26] animate-pulse rounded-full w-16 h-16 text-center absolute mt-10 3xl:top-[30%] 2xl:top-[30%] xl:top-[30%] lg:top-[30%] md:top-[%] sm:top-[74%] 3xl:ml-[29.5%] 2xl:ml-[29.5%] xl:ml-[29.5%] lg:ml-[29.5%] md:ml-[65%] sm:ml-[64%]">
                <h1 className="items-center text-3xl pt-3 text-green-700">E</h1>
              </div>
              <div className="h-12  w-56 bg-white shadow-sm shadow-slate-700 pt-3 text-sm text-center animate-bounce  absolute mt-12 3xl:top-[30%] 2xl:top-[30%] xl:top-[30%] lg:top-[30%] ml-[15%] font-inter md:hidden">
                <p>SMS-Channel Messaging</p>
              </div>
            </div>
            <div className="flex ">
              <div className="bg-[#6c63ff26] animate-pulse rounded-full w-16 h-16 text-center mt-6 ">
                <h1 className="items-center text-3xl pt-3 text-[#6C63FF]">B</h1>
              </div>
              <div className="h-12  w-56 bg-white text-sm animate-bounce shadow-sm shadow-slate-700 pt-3 text-center absolute mt-12 3xl:top-[50%] 2xl:top-[60%] xl:top-[60%] lg:top-[60%] md:top-[74%] sm:top-[74%] 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20 md:ml-40 sm:ml-40 font-inter">
                <p>Deliver messages efficiently.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="h-auto w-[83vw]  shadow-sm shadow-gray-500 pb-20 3xl:pt-24 2xl:pt-24 xl:pt-24 lg:pt-24 md:pt-12 sm:pt-12 3xl:ml-24 2xl:ml-24 xl:ml-24 lg:ml-12 md:ml-4 sm:ml-4 3xl:pl-24 2xl:pl-24 xl:pl-24 lg:pl-12 sm:pl-10 3xl:mt-[12%] 2xl:mt-[12%] xl:mt-[12%] lg:mt-[12%] md:mt-[22%] sm:mt-[22%] ">
        <div className="3xl:flex 2xl:flex xl:flex lg:flex md:block sm:block  md:pl-12 sm:pl-2">
          <div className="flex">
            <img src="icons/message.svg" alt="message" className="h-12 w-12 " />
            <div className="pl-6 font-karla">
              <h1>SMS-Channel Messaging</h1>
              <p>Use sms to verify, authenticate and engage customers.</p>
            </div>
          </div>
          <div className="flex 3xl:ml-40 2xl:ml-40 lg:ml-20   sm:ml-0 md:pt-10 sm:pt-10">
            <img
              src="icons/topupTick.svg"
              alt="message"
              className="h-12 w-12"
            />
            <div className="pl-6 font-karla">
              <h1 className="text-lg ">SMS-Channel Messaging</h1>
              <p>Use sms to verify, authenticate and engage customers.</p>
            </div>
          </div>
        </div>
        <div className="3xl:flex 2xl:flex xl:flex lg:flex md:block sm:block mt-16 md:pl-12 sm:pl-2">
          <div className="flex">
            <img src="icons/folder.svg" alt="message" className="h-12 w-12" />
            <div className="pl-6 font-karla">
              <h1>SMS-Channel Messaging</h1>
              <p>Use sms to verify, authenticate and engage customers.</p>
            </div>
          </div>
          <div className="flex 3xl:ml-40 2xl:ml-40 xl:ml-40 lg:ml-20 sm:ml-0 md:pt-10 sm:pt-10">
            <img src="icons/team.svg" alt="message" className="h-12 w-12" />
            <div className="pl-6 font-karla">
              <h1>SMS-Channel Messaging</h1>
              <p>Use sms to verify, authenticate and engage customers.</p>
            </div>
          </div>
        </div>
      </div>
      <div className="3xl:flex 2xl:flex xl:flex lg:flex md:block sm:block ">
        <div className="w-1/2  md:w-full sm:w-full mt-[10%]">
          <div className="3xl:pl-24 2xl:pl-24 xl:pl-24 lg:pl-12 md:pl-4 sm:pl-4 3xl:w-[35vw] 2xl:w-[35vw] xl:w-[35vw] lg:w-[35vw] md:w-full">
            <h1 className="text-[#6C63FF] text-3xl font-karla">
              Deliver messages efficiently.
            </h1>
            <p className="pt-4 font-inter text-black text-opacity-50">
              Enhance customer loyalty and retention by guaranteeing the
              delivery of bulk, group or single message.
            </p>
          </div>
        </div>
        <div className="w-1/2 md:w-[80vw] sm:w-[100vw]">
          <Image
            width={500}
            height={600}
            alt="signup image"
            src={'/images/form.png'}
            className="lg:ml-4  border-4 border-gray-300 border-solid rounded-lg md:ml-4 sm:ml-4 3xl:mt-32 2xl:mt-32 xl:mt-32 lg:mt-20 md:mt-10 sm:mt-12 lg:h-[40vh] lg:w-[40vw] sm:w-[80vw] sm:mt-20 sm:w-[38vw] sm:h-[38vh]"
          />
        </div>
      </div>
      <div className="3xl:flex 2xl:flex xl:flex lg:flex lg:pl-4 sm:pl-0 border-b-4 border-opacity-25 pb-4 border-[#6C63FF] border-solid">
        <div className="w-1/2 md:w-[80vw] sm:w-[100vw] ">
          <Image
            width={500}
            height={600}
            alt="signup image"
            src={'/images/chart.png'}
            className="3xl:ml-24 2xl:ml-24 xl:ml-24 lg:ml-4 md:ml-4 sm:ml-4 3xl:mt-32 2xl:mt-32 xl:mt-32 lg:mt-20 md:mt-10 sm:mt-12 lg:h-[40vh] lg:w-[40vw] sm:w-[80vw] sm:mt-20 sm:w-[38vw] sm:h-[38vh]"
          />
        </div>
        <div className=" md:w-full  sm:w-full mt-[10%]">
          <div className=" 2xl:pl-16 xl:pl-32 lg:pl-16 md:pl-4 sm:pl-0 lg:w-[35vw] lg:pt-8">
            <h1 className="text-[#6C63FF] text-3xl font-karla">
              Retrieve real-time reports.
            </h1>
            <p className="pt-4 font-inter text-black text-opacity-50">
              Generate detailed cross-channel reports for incoming and outgoing
              agent messages.
            </p>
          </div>
        </div>
      </div>
      <div className="3xl:flex 2xl:flex xl:flex lg:flex md:block sm:block pb-14 justify-around">
        <div className="mt-8">
          <img src="icons/logo.svg" alt="logo" />
          <p className="w-72 pt-12 font-inter text-black text-opacity-80">
            Termii helps businesses use messaging channels to verify and
            authenticate sms topups.
          </p>
        </div>
        <div className="mt-14 font-inter">
          <h1 className="text-2xl text-[#6C63FF]">Contact Us </h1>
          <p>Any questions? Talk to the admin.</p>
          <div className="flex mt-4">
            <Telephone className="text-[#6C63FF]" />
            <p className="pl-3 text-black text-opacity-80">+250785341151</p>
          </div>
          <div className="flex">
            <EnvelopeOpen className="text-[#6C63FF]" />
            <p className="pl-3 text-black text-opacity-80">supaad@gmail.com</p>
          </div>
        </div>
        <div className="w-80 mt-14 lg:block font-inter">
          <p>
            2022 Beep-in is focused on delivering great customer messaging
            experience globally.
          </p>
          <p className="text-[#6C63FF] pt-8">Thank you!</p>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
