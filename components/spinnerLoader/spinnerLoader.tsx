import React from 'react';
import useLoader from '../context/loaderContext';
import styles from './spinner.module.css';
function SpinnerLoader() {
  const { loading } = useLoader();
  return <div>{loading && <div className={styles.loader}></div>}</div>;
}

export default SpinnerLoader;
