import React, { useEffect, useState } from "react";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import Paginator from "../utils/paginator";
import { CircleFill } from "react-bootstrap-icons";
import { useRouter } from "next/router";
import { api } from "../utils/api";
import useResponse from "../context/responseContext";
function ViewApi() {
  const [tableData, setTableData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [current, setCurrent] = useState<[]>([]);
  const {updateSuccess, updateMessage, updateFailure} = useResponse()
  const router = useRouter();
  const { type } = router.query;

  useEffect(() => {
    api.get("/api/get").then((res) => {
      setTableData(res.data.data);
    });
  }, []);

  return (
    <div className="w-full font-karla">
      <div className="h-20 w-full border-b-2 border-solid flex justify-center float-right items-center text-[#6C63FF] text-xl ">
        <h1>SMS APIS</h1>
      </div>
      {type == "viewApis" && (
        <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[75vw] lg:w-[70vw] mt-20 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
          <div className="flex flex-col mt-4">
            <div className="overflow-x-auto">
              <div className=" w-full inline-block align-middle">
              <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100 overflow-x-auto overflow-hidden">
                <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                    <thead className="bg-gray-50">
                      <tr>
                        <th scope="col" className="px-6 py-3  text-left  ">
                          Company
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Balance
                        </th>
                        <th scope="col" className="px-6 py-3  text-left">
                          Status
                        </th>
                        <th scope="col" className="px-6 py-3  text-left">
                          Set as default
                        </th>
                        <th scope="col" className="px-6  text-left">
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 font-inter">
                      {tableData.map((item: any) => {
                        return (
                          <tr>
                            <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {item.name}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap ">
                              {item.balance}
                            </td>
                            <td className="px-6 py-6 text-sm font-medium text-left  whitespace-nowrap flex">
                              {item.enabled ? "Integrated" : "Not integrated"}
                              <CircleFill
                                className={`${
                                  item.enabled
                                    ? "text-green-400"
                                    : "text-red-400"
                                } ml-4 mt-1 text-xs`}
                              />
                            </td>
                            <td className="px-6 py-4 text-sm font-medium text-left whitespace-nowrap">
                              {item.setDefault ? (
                                <button className="h-10 w-auto rounded-lg bg-green-100 bg-opacity-10 text-green-400 pl-6 pr-6">
                                  Enabled
                                </button>
                              ) : (
                                <button className="h-10 w-auto rounded-lg bg-[#8A0000] bg-opacity-10 text-[#8A0000] pl-6 pr-6">
                                  DISABLED
                                </button>
                              )}
                          
                            </td>

                            <td className="px-6">
                              <div className="flex">
                                <label className="inline-flex relative cursor-pointer">
                                  <input
                                    type="checkbox"
                                    value=""
                                    className="sr-only peer"
                                    onClick={()=>{
                                      api.post(`/api/setDefault/${item.id}`).then((res :any)=>{
                                        updateSuccess();
                                        updateMessage(res.data.message)
                                      }).catch((err:any)=>{
                                        updateFailure()
                                        updateMessage(err.data.message)
                                      })
                                    }}
                                  />
                                  <div className="w-10 h-5 bg-green-500 rounded-full peer peer-focus:ring-4 peer-focus:ring-white dark:peer-focus:ring-green-800 dark:bg-green-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[5px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-red-600"></div>
                                </label>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="ml-auto mr-auto">
              {isLoading ? <SpinnerLoader /> : null}
            </div>
          </div>
          {/* <Paginator numPages={6} data={tableData} setCurrent={setCurrent} /> */}
        </div>
      )}
    </div>
  );
}

export default ViewApi;
