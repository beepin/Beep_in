import { useState, useReducer, useEffect } from "react";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import Image from "next/image";
import Link from "next/link";
import Paginator from "../utils/paginator";
import useResponse from "../context/responseContext";

import {
  XCircle,
  Filter,
  Trash3,
  PlusCircle,
  CloudDownload,
  PencilSquare,
  Search,
} from 'react-bootstrap-icons';
import { AiOutlineEye, AiOutlinePlus } from 'react-icons/ai';
import { useForm } from 'react-hook-form';
import { api } from '../utils/api';
import { exportTableToCSV } from '../utils/download';
import {useRouter} from 'next/router'
import useStateCallback from '../utils/useStateCallback';
import useLoader from "../context/loaderContext";
type ACTIONTYPE =
  | { type: "newGroup" }
  | { type: "typeContacts" }
  | { type: "uploadFile" }
  | { type: "success" }
  | { type: "groupList" }
  | { type: "groupIcon" };
const initialMethod = {
  typeContacts: false,
  uploadFile: false,
};
const initialEvent = {
  success: false,
  groupIcon: true,
};

interface FormValues {
  id: string;
  name: string;
  members: string;
  tel: number;
}
interface groupValues {
  id: string;
  name: string;
  groupMembers: Array<any>;
}

interface MemberData {
  id: string;
  group_id: string;
  tel: number;
}
const eventDisplay = (eventHappen: typeof initialEvent, action: ACTIONTYPE) => {
  eventHappen = {
    success: false,
    groupIcon: false,
  };
  switch (action.type) {
    case "groupIcon":
      return {
        ...eventHappen,
        groupIcon: true,
      };

    case "success":
      return {
        ...eventHappen,
        success: true,
      };
    default:
      return {
        ...eventHappen,
      };
  }
};
const methodType = (method: typeof initialMethod, action: ACTIONTYPE) => {
  method = {
    typeContacts: false,
    uploadFile: false,
  };
  switch (action.type) {
    case "typeContacts":
      return {
        ...method,
        typeContacts: true,
      };
    case "uploadFile":
      return {
        ...method,
        uploadFile: true,
      };
    default:
      return {
        ...method,
      };
  }
};

function GroupsManagement() {
  const [method, setDisplayMethod] = useReducer(methodType, initialMethod);
  const [createGroup, setCreateGroup] = useState(true);
  const [eventHappen, dispatch] = useReducer(eventDisplay, initialEvent);
  const { register, handleSubmit } = useForm<FormValues>();
  const [memberData,setMemberData] = useState<string>()
  const [tableData, setTableData] = useStateCallback<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [files, setFiles] = useState<File[] | null>([]);
  const { updateSuccess, updateMessage, updateFailure } = useResponse();
  const [addMember, setAddMember] = useState<Boolean>(false);
  const [current, setCurrent] = useStateCallback<[]>([]);
  const [searchedVal, setSearchedVal] = useState("");
  const router = useRouter();
  const {loading, updateLoading} = useLoader();

  const submit = (data: FormValues) => {
    if (createGroup && method.uploadFile) {
      const formData = new FormData();
      formData.append("groups", files![0]);
      formData.append("name", data.name);
      api
        .post("/groups/create/file", formData, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        .then((response) => {
          updateSuccess()
          updateMessage(response.data.message)
        });
    } else {
      api
        .post("/groups/create", data)
        .then((res) => {
          updateSuccess()
          updateMessage(res.data.message)
        })
        .catch((err) => {
          updateFailure()
          updateMessage(err.message)
        }); 
    }
    getGroups();
  };
  const getGroups = () => {
    setIsLoading(true);
    const url = "/groups/get";
    api.get(url).then((res) => {
      setTableData(res.data.data,(groups:any)=>{
        setCurrent(groups!.slice(0,5));
      })
      updateLoading(false);
    });
  };
  useEffect(() => {
    getGroups();
  }, []);

  const editGroupMembers = async (values: FormValues) => {
    setMemberData(JSON.stringify(values));
    await api
      .post("groups/add", values)
      .then((response) => {
        updateSuccess()
        updateMessage(response.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let { type } = router.query;

  return (
    <div className="w-full">
      <div className="h-20 w-full border-b-2 border-solid flex 3xl:justify-center 2xl:justify-center xl:justify-center lg:justify-evenly  float-right items-center text-[#6C63FF] font-karla">
        <h1 className=" 3xl:pr-0 2xl:pr-0 xl:pr-0 lg:pr-40  sm:pl-10 md:pl-14">
          Recipients Groups
        </h1>
        {/* <button className="flex bg-gray-100 h-12 3xl:w-32 2xl:w-32 xl:w-32 lg:w-32 md:w-32 sm:w-12 sm:pl-1 sm:text-xl text-center rounded-lg 3xl:absolute 2xl:absolute xl:absolute lg:absolute md:absolute sm:absolute z-0 3xl:right-64 2xl:right-64 xl:right-52 lg:right-52 md:right-48 sm:right-32 pl-4 w-32 items-center md:ml-8 font-karla">
          <p className="sm:hidden">Filter by </p>
          <Filter className="text-[#6C63FF]  ml-2 mt-1" />
        </button> */}
        <button
          onClick={() => {
            setCreateGroup(true);
          }}
          className="flex bg-[#6C63FF] text-white items-center text-center gap-3 absolute right-10 h-12 3xl:w-32 2xl:w-32 xl:w-32 lg:w-32 md:w-32 sm:w-12 sm:pl-3 sm:text-xl pl-2 font-karla rounded-lg hover:bg-blue-400"
        >
          <Link
            href={{ pathname: "groupManagement", query: { type: "newGroup" } }}
            className="flex"
          >
            <AiOutlinePlus className="mt-1 mr-1" />
            <p className="sm:hidden">New group</p>
          </Link>
        </button>
      </div>
      {createGroup && type == "newGroup" && (
        <div className="h-full w-full bg-gray-800 opacity-90 absolute top-0 left-0">
          {/* {eventHappen.newGroup && ( */}
          <div className="3xl:h-[74vh] 2xl:h-[74vh] xl:h-[74vh] lg:h-[74vh] md:h-[74vh] sm:h-[74vh]  3xl:w-1/3 2xl:w-1/3 xl:w-2/3 lg:w-1/2 md:w-3/4 sm:w-3/4 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
            <XCircle
              className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
              onClick={() => {
                router.push("/dashboard/groupManagement");
              }}
            />
            <img
              src="/icons/group.svg"
              alt="topup-tick"
              className="pt-6 ml-auto mr-auto "
            />
            <h1 className="text-center font-bold text-2xl font-karla">
              Create a group
            </h1>
            <form
              action=""
              className="ml-16 sm:ml-8 mt-10"
              onSubmit={handleSubmit(submit)}
            >
              <input
                {...register("name")}
                type="text"
                name="name"
                placeholder="Group name"
                required
                className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 font-inter"
              />
              <p className="pt-8 font-bold font-karla">
                What kind of method do you prefer?
              </p>
              <div className="flex items-center mt-3">
                <input
                  onClick={() => setDisplayMethod({ type: "typeContacts" })}
                  id="default-radio-2"
                  type="radio"
                  value="typeContact"
                  name="default-radio"
                  className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600 pl-8"
                />
                <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                  Type contacts
                </label>
                <div className="flex items-center">
                  <input
                    onClick={() => setDisplayMethod({ type: "uploadFile" })}
                    id="default-radio-2"
                    type="radio"
                    value="uploadFile"
                    name="default-radio"
                    className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600 ml-10"
                  />
                  <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                    Upload a file
                  </label>
                </div>
              </div>

              <div className="block">
                {method.typeContacts && (
                  <textarea
                    {...register("members")}
                    className="h-24 mt-4 w-5/6 min-h-24 max-h-24 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600 pl-8 pt-4 rounded-lg font-karla"
                    placeholder="Type telephone numbers of group members separated with commas and no space"
                  ></textarea>
                )}
                {method.uploadFile && (
                  <div className="flex mt-4 w-full">
                      <div className="flex mt-4 w-full">
                    <label className="flex flex-col items-center justify-center w-5/6  h-24 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600 font-inter">
                      <input
                        type="file"
                        name="receiver"
                        id="dropzone-file"
                        className="ml-32 sm:ml-16 sm:text-sm"
                        accept={".csv"}
                        onChange={(e) => {
                          const target = e.target as HTMLInputElement;
                          setFiles((target!.files || null) as File[] | null);
                        }}
                      />
                    </label>
                  </div>
                  </div>
                )}
              </div>
              <button
                type="submit"
                className="float-right text-center mr-20 mt-4 items-center font-normal font-karla bg-[#6C63FF] h-12  w-32 rounded-lg text-white "
              >
                Create
              </button>
            </form>
          </div>
          {/* )}  */}
        </div>
      )}
      {eventHappen.success && (
        <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
          <XCircle
            className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
            onClick={() => setCreateGroup(false)}
          />
          <img
            src="/icons/sucess.svg"
            alt="topup-tick"
            className="ml-auto mr-auto pt-28"
          />
          <h1 className="text-center pt-8 text-xl font-karla">
            Start Sending Messages to The Nickels
          </h1>
          <button className="h-12 w-40 bg-[#6C63FF] text-white ml-48 rounded-lg mt-10 font-inter">
            PROCEED
          </button>
        </div>
      )}

      {type == "groupsList" && (
        <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[75vw] lg:w-[70vw] mt-20 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
             <form className="flex items-center 3xl:w-1/4 2xl:w-1/4 xl:2/3 mt-4 lg:ml-6 md:ml-6 sm:ml-6 lg:mr-4 md:mr-4 sm:mr-4">
            <label className="sr-only">Search</label>
            <div className="relative w-full">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <Search className="text-[#6C63FF]" />
              </div>
              <input
                type="text"
                id="simple-search"
                className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                placeholder="Search by name"
                required
                onChange={(e) => setSearchedVal(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              <Search />
              <span className="sr-only">Search</span>
            </button>
         
          </form>
        
          <div className="flex flex-col mt-4">
            <div className="overflow-x-auto">
              <div className=" w-full inline-block align-middle">
              <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100  overflow-x-auto overflow-hidden">
                <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                    <thead className="bg-gray-50">
                      <tr>
                        <th scope="col" className="px-6 py-3  text-left  ">
                          Group Name
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Number of People
                        </th>
                        <th scope="col" className="px-6 py-3  text-left">
                          Add People
                        </th>
                        <th scope="col" className="px-6 py-3  text-left">
                          Download
                        </th>
                        <th scope="col" className="px-6  text-left">
                          More
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 font-inter">
                      {current
                        .filter((row: groupValues) => {
                          if (!searchedVal.length) {
                            return true; // return all rows if search value is not provided
                          }
                          const name = row.name
                            ? row.name.toString().toLowerCase()
                            : ""; // convert name to string and lowercase it
                          const searched = searchedVal.toString().toLowerCase(); // convert search value to string and lowercase it
                          return name.includes(searched); // return true if name includes the search value
                        })
                        .map((sms_data: groupValues) => (
                          <tr>
                            <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {sms_data.name}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap flex">
                              {sms_data.groupMembers.length}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                              <Link
                                onClick={() => setAddMember(true)}
                                href={{
                                  pathname: "groupManagement",
                                  query: {
                                    type: "addMember",
                                    groupid: sms_data.id,
                                  },
                                }}
                              >
                                <PlusCircle className="text-xl text-[#6C63FF]" />
                              </Link>
                            </td>
                            <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                              <CloudDownload
                                className="text-xl text-[#6C63FF]"
                                onClick={() => {
                                  let row: string[] = [];
                                  for (
                                    let i = 0;
                                    i < sms_data.groupMembers.length;
                                    i++
                                  ) {
                                    row.push(sms_data.groupMembers[i].tel);
                                  }
                                  exportTableToCSV(row, `${sms_data.name}.csv`);
                                }}
                              />
                            </td>
                            <td className="px-6 text-sm font-medium text-right right-4  gap-2  whitespace-nowrap flex">
                              <Link
                                href={{
                                  pathname: "groupManagement",
                                  query: {
                                    type: "viewGroup",
                                    groupid: sms_data.id,
                                    groupname:sms_data.name
                                  },
                                }}
                              >
                                <AiOutlineEye
                                  className="text-green-500 text-xl"
                                />
                              </Link>
                              <Trash3
                                className="text-red-500 text-xl"
                                onClick={async () => {
                                  await api
                                    .post(`/groups/delete/${sms_data.id}`)
                                    .then((res) => {
                                      updateSuccess();
                                      updateMessage(res.data.message);
                                      setTableData(res.data);
                                    })
                                    .catch((err) => {
                                      updateFailure();
                                      updateMessage(err.message);
                                    });
                                }}
                              />
                              {/* <PencilSquare className="text-[#968D8D]" /> */}
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="ml-auto mr-auto">
              {isLoading ? <SpinnerLoader /> : null}
            </div>
          </div>
          <Paginator numPages={6} data={tableData} setCurrent={setCurrent} />
        </div>
      )}
      {type == "addMember"
        ? type == "addMember"
        : type == "newGroup"
        ? type == "newGroup"
        : type == "groupsList"
        ? type == "groupsList"
        : type == "viewGroup"
        ? type == "viewGroup"
        : eventHappen.groupIcon && (
            <div className="h-96 mt-28 3xl:absolute z-0 2xl:flex 2xl:justify-center xl:flex xl:justify-center  lg:flex lg:justify-center  right-[35%] md:flex md:justify-center sm:flex sm:justify-center ">
              <Image
                width={400}
                height={400}
                alt="signup image"
                src={"/images/groupsImage.png"}
                className="mt-12 h-[60vh]"
              />
            </div>
          )}
      {type == "viewGroup" && (
        <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[75vw] lg:w-[70vw] mt-20 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
          <div className="flex flex-col mt-4">
            <div className="overflow-x-auto">
              <div className=" w-full inline-block align-middle">
                <div className="overflow-hidden w-full">
                  <table className="min-w-full divide-y divide-gray-200 font-karla">
                    <thead className="bg-gray-50">
                      <tr>
                        <th scope="col" className="px-20 py-3 text-left ">
                          Agent Id
                        </th>
                        <th scope="col" className="px-16 py-3 text-left">
                          Mobile Number
                        </th>
                        <th scope="col" className="px-16 py-3  text-left">
                          Group id
                        </th>

                        <th scope="col" className="px-16  text-left">
                          Actions
                        </th>
                      </tr>
                    </thead>

                    {current?.map((group_data: groupValues) => (
                      <>
                        {group_data.groupMembers.map(
                          (members_data: MemberData) => (
                            <tbody className="divide-y divide-gray-200 font-inter">
                              {members_data.group_id == router.query.groupid ? (
                                <tr>
                                  <td className="px-20 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                    {members_data.id}
                                  </td>

                                  <td className="px-16 py-4 text-sm text-gray-800 whitespace-nowrap flex">
                                    {members_data.tel}
                                  </td>
                                  <td className="px-16 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {router.query.groupname}
                                  </td>

                                  <td className="px-16 text-sm font-medium text-right right-4  gap-2  whitespace-nowrap flex">
                                    <button
                                      className="h-10 w-auto rounded-lg bg-[#8A0000] bg-opacity-10 text-[#8A0000] pl-6 pr-6"
                                      onClick={async () => {
                                        await api
                                          .post(
                                            `groups/member/delete/${members_data.id}`
                                          )
                                          .then((res: any) => {
                                            updateSuccess();
                                            updateMessage(res.message);
                                            setTableData(res.data);
                                          })
                                          .catch((err) => {
                                            updateFailure();
                                            updateMessage(err.message);
                                          });
                                      }}
                                    >
                                      Remove
                                    </button>
                                  </td>
                                </tr>
                              ) : (
                                ""
                              )}
                            </tbody>
                          )
                        )}
                      </>
                    ))}
                  </table>
                </div>
              </div>
            </div>
            <div className="ml-auto mr-auto">
              {isLoading ? <SpinnerLoader /> : null}
            </div>
          </div>
          <Paginator numPages={6} data={tableData} setCurrent={setCurrent} />
        </div>
      )}
      {addMember && type == "addMember" && (
        <div className="h-full w-full bg-gray-800 opacity-90 absolute top-0 left-0">
          <div className="3xl:h-[40vh] 2xl:h-[60vh] xl:h-[60vh] lg:h-[60vh] md:h-[60vh] sm:h-[60vh]  3xl:w-1/3 2xl:w-1/3 xl:w-2/3 lg:w-1/2 md:w-3/4 sm:w-3/4 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
            <XCircle
              className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
              onClick={() => {
                router.push("/dashboard/groupManagement");
              }}
            />

            <h1 className="text-center text-[#6C63FF] pt-16 font-bold text-2xl font-karla">
              {router.query.groupname}
            </h1>

            <p className="text-center font-karla">Add new member</p>
            <form
              action=""
              className="ml-16 sm:ml-8 mt-10"
              onSubmit={handleSubmit(editGroupMembers)}
            >
              <input
                {...register("tel")}
                type="telephone"
                name="tel"
                placeholder="Mobile number"
                required
                className=" block border-solid border bg-[#D9D9D9]  mt-6 h-14  w-5/6 rounded-lg pl-8 font-inter"
              />
              <div className="mt-10 ">
                <button
                  type="button"
                  className="float-left text-center  mt-4 items-center font-normal font-karla bg-white shadow-md shadow-gray-100 text-[#6C63FF] h-12  w-32 rounded-lg "
                >
                  <Link
                    href={{
                      pathname: "groupManagement",
                      query: { type: "groupsList" },
                    }}
                  >
                    CANCEL
                  </Link>
                </button>
                <button
                  type="submit"
                  className="float-right text-center mr-20  mt-4 items-center font-normal font-karla bg-[#6C63FF] h-12  w-32 rounded-lg text-white "
                >
                  SAVE
                </button>
              </div>
            </form>
          </div>

          {/* )}  */}
        </div>
      )}
    </div>
  );
}

export default GroupsManagement;
