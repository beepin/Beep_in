import Link from "next/link";

import { Download, Search, Smartwatch, X } from "react-bootstrap-icons";
import { BiChevronRightCircle, BiChevronLeftCircle } from "react-icons/bi";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import React, { useEffect, useReducer, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import useAuth from "../context/authContext";
import useResponse from "../context/responseContext";
import { api } from "../utils/api";
import io from "socket.io-client";
import { useRouter } from "next/router";
import { getCookie } from "cookies-next";
import Paginator from "../utils/paginator";
import useStateCallback from "../utils/useStateCallback";
interface FormValues {
  agent: string;
  agent_id: string;
  name: string;
  messages: number;
}

const initialMode = {
  momo: false,
  bank: false,
  card: false,
  allMethods: false,
  complete: false,
  topupData: true,
  issueTopup: false,
};
const initialType = {
  typeContact: false,
  uploadFile: false,
};
type ACTIONTYPE =
  | { type: "momo" }
  | { type: "bank" }
  | { type: "card" }
  | { type: "allMethods" }
  | { type: "complete" }
  | { type: "typeContact" }
  | { type: "uploadFile" }
  | { type: "topupData" };

const paymentMode = (mode: typeof initialMode, action: ACTIONTYPE) => {
  mode = {
    momo: false,
    bank: false,
    card: false,
    allMethods: false,
    complete: false,
    topupData: false,
    issueTopup: false,
  };
  switch (action.type) {
    case "topupData":
      return {
        ...mode,
        topupData: true,
      };
    // case 'issueTopup':
    //   return {
    //     ...mode,
    //     issueTopup: true,
    //   };
    case "allMethods":
      return {
        ...mode,
        allMethods: true,
      };
    case "momo":
      return {
        ...mode,
        momo: true,
      };

    case "bank":
      return {
        ...mode,
        bank: true,
      };

    case "card":
      return {
        ...mode,
        card: true,
      };
    case "complete":
      return {
        ...mode,
        complete: true,
      };
    default:
      return {
        ...mode,
      };
  }
};

type Topup = {
  created_at: string;
  total_price: number;
  status: string;
  unit_price: number;
  RMS: number;
  paid: number;
};

enum filter {
  APPROVED = "APPROVED",
  DECLINED = "DECLINED",
  PENDING = "REQUESTED",
  ALL = "ALL",
}
function Topup(): JSX.Element {
  const [mode, dispatch] = useReducer(paymentMode, initialMode);
  const { register, handleSubmit } = useForm<FormValues>();
  const { user } = useAuth();
  const [files, setFiles] = useState<File[] | null>([]);
  const [searchedVal, setSearchedVal] = useState("")
  const { updateSuccess, updateMessage, updateFailure } = useResponse();
  const socketUrl = process.env.NEXT_PUBLIC_BEEP_IN_SOCKET_URL;
  const socket = useRef(io(socketUrl!));
  const [tableData, setTableData] = useStateCallback<any>([{}, {}, {}]);
  const [current, setCurrent] = useStateCallback<[]>([]);

  const [utils, setUtils] = useReducer(
    (prev: any, next: any) => {
      return { ...prev, ...next };
    },
    {
      isLoading: false,
      tableData: [],
      filterVal: filter.ALL,
      requests: [],
      showPopup: true,
      topupMenu: [
        { id: 1, privilege: [4, 3], menu: "Top up history" },
        { id: 2, privilege: [4, 3], menu: "Buy sms" },
        { id: 3, privilege: [1, 2, 3], menu: "Top up Report" },
        { id: 4, privilege: [1, 2, 3], menu: "Issue Top Up" },
      ],
      agents: [],
    }
  );

  const submit = (data: FormValues) => {
    // if (utils.showPopup && mode.bank) {
    //   const formData = new FormData();
    //   formData.append("file", files![0]);
    //   const url = `topup/uploadProof/p989`;
    //   api
    //     .post(url, formData, {
    //       headers: {
    //         "Content-Type": "application/json",
    //         authorization: "Bearer " + getCookie("accessToken"),
    //       },
    //     })
    //     .then((res) => {
    //       updateSuccess();
    //       updateMessage(res.data.data);
    //     });
    // }
    const url = mode.topupData ? `/topup/request` : `/topup/create`;
    socket.current.emit("createTopup", {
      receiver: data.agent_id,
      sender: user?.id,
      message: "Create Topup request",
    });
    socket.current.emit("Check", { data: "No data" });
    api
      .post(url, data)
      .then((res) => {
        updateSuccess();
        updateMessage(res.data.message);
        dispatch({ type: "allMethods" });
      })
      .catch((err) => {
        updateFailure();
        updateMessage(err.message);
      });
  };
  const getTopupRequests = () => {
    api.get("topup/get/ALL").then((res) => {
      
      setTableData(res.data.data, (topup: any) => {
       
        setCurrent(topup?.slice(0, 5));
        
      })
      updateSuccess();
      updateMessage(res.data.message);
        
    });
  };
  const getAgents = () => {
    api.get("/agent/all").then((res) => {
      setUtils({ agents: res.data.agents });
    });
  };
  const getTopupReport = () => {
    setUtils({ isLoading: true });
    api
      .get(`/topup/report/${utils.filterVal}`, {
        headers: {
          authorization: "Bearer " + getCookie("accessToken"),
        },
      })
      .then((res) => {
        setTableData(res.data.data, (topup: any) => {
       
          setCurrent(topup?.slice(0, 5));
          
        })

        updateSuccess();
        updateMessage(res.data.message);
      })
      .catch((err) => {
        updateFailure();
        updateMessage(err.message);
      });
  };
  useEffect(() => {
    setUtils({ isLoading: false });

  }, [utils.tableData])
  useEffect(() => {
    const menu = utils.topupMenu.filter((item: any) => {
      return item.privilege.includes(user?.position_id as number);
    });
    setUtils({ topupMenu: menu });
    user?.position_id != 1 ? getTopupRequests() :  getTopupReport();
  }, [utils.filterVal]);
  useEffect(() => {
    socket.current = io(socketUrl!);
    socket.current.on("connected", (data) => {

    })
    socket.current.emit("addUser", { userId: user?.id });
  }, []);

  const updateFilter = (e: any) => {
    setUtils({ filterVal: e.target.value as filter });
  };

  const status = ["APPROVE", "DECLINE"];
  const router = useRouter();
  const { type } = router.query;

  function createTopup() {
    getAgents();
    setUtils({ showPopup: true });
  }
  
  return (
    <div className="w-full">
      <div className="h-20 w-full border-b-2 border-solid flex 3xl:justify-center 2xl:justify-center xl:justify-center    items-center text-[#6C63FF] font-karla">
        <h1 className="text-lg md:pl-6 sm:pl-6">Topup History</h1>

        <select
          className="flex bg-gray-100 h-12 text-center rounded-lg absolute  3xl:right-64 2xl:right-64 xl:right-64 lg:right-72 md:right-52  pl-4 w-32 items-center font-karla"
          onChange={updateFilter}
        >
          <option value="ALL">Filter</option>
          <option value={filter.APPROVED}>Approved</option>
          <option value={filter.PENDING}>Pending</option>
          <option value={filter.ALL}>All</option>
          <option value={filter.DECLINED}>Declined</option>
        </select>
        <button
          className="flex bg-white text-[#6C63FF] items-center text-center gap-3 absolute right-10 font-karla"
        >
          Download Report <Download />
        </button>
      </div>

      {type == "topupHistory" && (
        <div className="h-96 w-3/4 mt-28 absolute lg:right-10 md:right-2 sm:right-1">
          <form className="flex items-center 3xl:w-1/4 2xl:w-1/4 xl:2/3 mt-4 lg:ml-6 md:ml-6 sm:ml-6 lg:mr-4 md:mr-4 sm:mr-4">
            <label className="sr-only">Search</label>
            <div className="relative w-full">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <Search className="text-[#6C63FF]" />
              </div>
              <input
                type="text"
                id="simple-search"
                className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                placeholder="Search by name"
                required
                onChange={(e) => setSearchedVal(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              <Search />
              <span className="sr-only">Search</span>
            </button>
          </form>
          <div className="flex flex-col mt-4">
            <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[75vw] lg:w-[70vw] mt-20 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
              <div className="p-1.5 lg:w-full md:w-[40vw] lg:ml-0 md:ml-10 inline-block align-middle h-[40vh]">
                <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100 h-32 overflow-x-auto overflow-hidden">
                  <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                    <thead className="bg-gray-50 font-karla">
                      <tr>
                        <th scope="col" className=" px-6 py-3 text-left ">
                          Created at
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Buying price
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Loaded messages
                        </th>

                        <th scope="col" className="px-6 py-3 text-left ">
                          Total amount
                        </th>
                        <th scope="col" className="px-6 py-3 text-left ">
                          Status
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 font-inter h-[40vh]">

                      {utils.tableData
                        .filter((row: any) => {
                          if (!searchedVal.length) {
                            return true; // return all rows if search value is not provided
                          }
                          const rms = row.RMS
                            ? row.RMS.toString().toLowerCase()
                            : ""; 
                          const searched = searchedVal.toString().toLowerCase(); // convert search value to string and lowercase it
                          return rms.includes(searched); // return true if name includes the search value
                        })
                        .map((sms_data: any) => (
                          <tr>
                            <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {sms_data.created_at.split("T")[0]}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap flex">
                              {sms_data.unit_price}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                              {sms_data.RMS}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                              {sms_data.total_price}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                              {sms_data.status}
                            </td>
                            {/* 
                          <td className="px-6 py-4 text-sm font-medium text-right  gap-2  whitespace-nowrap">
                            <Link href="requestTopup">
                              <button className="flex bg-[#4CAF50] bg-opacity-5 text-green-500 h-8 w-24 lg:pl-4 pt-2 rounded-lg">
                                <p className="pr-2">Balance</p>
                                <AiOutlineEye />
                              </button>
                            </Link>
                          </td> */}
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="ml-auto mr-auto">
              {utils.isLoading ? <SpinnerLoader /> : null}
            </div>
          </div>
          <div className="flex justify-center mt-2">
            <BiChevronLeftCircle className="text-3xl text-[#6C63FF]" />
            <BiChevronRightCircle className="text-3xl text-[#6C63FF]" />
          </div>
        </div>
      )}
      {type == "topupReport" && (
        <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[75vw] lg:w-[70vw] mt-8 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
          <form className="flex items-center 3xl:w-1/4 2xl:w-1/4 xl:2/3 mt-4 lg:ml-6 md:ml-6 sm:ml-6 lg:mr-4 md:mr-4 sm:mr-4">
            <label className="sr-only">Search</label>
            <div className="relative w-full">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <Search className="text-[#6C63FF]" />
              </div>
              <input
                type="text"
                id="simple-search"
                className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                placeholder="Search by name"
                required
                onChange={(e) => setSearchedVal(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              <Search />
              <span className="sr-only">Search</span>
            </button>
          </form>
          <div className="flex flex-col mt-4 h-[60vh]">
            <div className="overflow-x-auto">
              <div className="p-1.5 w-full inline-block align-middle">
                <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100 h-32 overflow-x-auto overflow-hidden">
                  <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                    <thead className="bg-gray-50">
                      <tr>
                        <th scope="col" className=" px-6 py-3 text-left ">
                          Created at
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Agent Name
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Phone Number
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Loaded messages
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Unit Price
                        </th>

                        <th scope="col" className="px-6 py-3 text-left">
                          Total Price
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Payment Mode
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Status
                        </th>
                        <th scope="col" className="px-6 py-3 text-left">
                          Paid
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 font-inter">
                      {current
                        .filter((row: any) => {
                          if (!searchedVal.length) {
                            return true; // return all rows if search value is not provided
                          }
                          const name = row.name
                            ? row.name.toString().toLowerCase()
                            : ""; // convert name to string and lowercase it
                          const searched = searchedVal.toString().toLowerCase(); // convert search value to string and lowercase it
                          return name.includes(searched); // return true if name includes the search value
                        })
                        .map((item: any) => (
                          <tr>
                            <td className="px-6 py-5 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {item.created_at.split("T")[0] + " " + item.created_at.split("T")[1].split(".")[0] }
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.name}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.phone_number}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.RMS}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.unit_price}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.RMS * item.unit_price}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.payment_mode}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.status}
                            </td>
                            <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                              {item.paid ? "PAID" : "NOT PAID"}
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="ml-auto mr-auto">
              {utils.isLoading ? <SpinnerLoader /> : null}
            </div>
          </div>
          <Paginator
            numPages={6}
            data={utils}
            setCurrent={setUtils}
          />
        </div>
      )}

      {type == "issuingTopup" && utils.showPopup && (
        <div className="h-full w-full bg-gray-800 opacity-90 absolute top-0 left-0">
          {mode.topupData && (
            <div className="3xl:h-[74vh] 2xl:h-[74vh] xl:h-[74vh] lg:h-[74vh] md:h-[74vh] sm:h-[74vh]  3xl:w-1/3 2xl:w-1/3 xl:w-2/3 lg:w-1/2 md:w-3/4 sm:w-3/4 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => {
                  router.push("dashboard/topUpDetails");
                  setUtils({ showPopup: false })
                }}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="pt-6 ml-auto mr-auto"
              />
              <h1 className="font-bold text-xl pt-6 text-center font-karla">
                Topup
              </h1>
              <form className="ml-16 mt-10" onSubmit={handleSubmit(submit)}>
                <input
                  {...register("name")}
                  type="text"
                  name="nameeofagent"
                  placeholder="Name of agent"
                  required
                  value={user?.name || ""}
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 font-karla"
                />
                <input
                  type="text"
                  name="mobilrnumber"
                  placeholder="Mobile number"
                  value={user?.phone_number || ""}
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-6 font-karla"
                />
                <input
                  {...register("messages")}
                  type="number"
                  name="messages"
                  placeholder="Number of SMS"
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-6 font-karla"
                />
                <input
                  {...register("agent_id")}
                  type="agent_id"
                  name="agent_id"
                  value={user?.created_by || ""}
                  className="hidden"
                />
                <button
                  // onClick={() => {dispatch({ type: "allMethods" })}}
                  type="submit"
                  className="flex float-right mr-20 mt-16 items-center font-normal font-karla"
                >
                  Payment method
                  <BiChevronRightCircle className="text-4xl text-[#6C63FF] ml-4" />
                </button>
              </form>
            </div>
          )}

          {mode.issueTopup && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="absolute top-14 ml-[14vw]"
              />
              <h1 className="font-bold text-xl pt-14 text-center font-karla">
                Topup
              </h1>
              <form className="ml-16 mt-10" onSubmit={handleSubmit(submit)}>
                <select
                  {...register("agent")}
                  name="agent"
                  placeholder="Agent"
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-2/3 rounded-lg pl-8 ml-20 font-inter"
                >
                  <option value="">Group Name</option>
                  {utils.agents!.map((agent: any) => {
                    return <option value={agent.id}>{agent.name}</option>;
                  })}
                </select>
                <input
                  type="text"
                  name="mobilrnumber"
                  placeholder="Mobile number"
                  value={user?.phone_number || ""}
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-6 font-karla"
                />
                <input
                  {...register("messages")}
                  type="number"
                  name="messages"
                  placeholder="Number of SMS"
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-6 font-karla"
                />
                <input
                  {...register("agent_id")}
                  type="agent_id"
                  name="agent_id"
                  value={user?.created_by || ""}
                  className="hidden"
                />
                <button
                  onClick={() => {
                    dispatch({ type: "allMethods" });
                  }}
                  type="submit"
                  className="h-14 w-28 bg-[#6C63FF] text-white rounded-xl "
                >
                  SAVE
                </button>
              </form>
            </div>
          )}

          {mode.allMethods && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="absolute top-14 ml-[14vw]"
              />
              <h1 className="font-bold text-xl pt-20 text-center font-karla">
                Payment method
              </h1>

              <div>
                <form action="" className="ml-16 mt-10">
                  <div className="flex items-center mb-4 mt-14">
                    <input
                      type="radio"
                      value="mtnmomo"
                      name="default-radio"
                      className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600"
                      // onChange={radioHandler}
                      onClick={() => dispatch({ type: "momo" })}
                    />
                    <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                      MTN momo
                    </label>
                  </div>
                  <div className="flex items-center mt-10">
                    <input
                      id="default-radio-2"
                      type="radio"
                      value="bank"
                      name="default-radio"
                      className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300   dark:bg-gray-700 dark:border-gray-600"
                      onClick={() => dispatch({ type: "bank" })}
                    />
                    <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                      Bank Payment
                    </label>
                  </div>
                  <div className="flex items-center mt-10">
                    <input
                      id="default-radio-2"
                      type="radio"
                      value="card"
                      name="default-radio"
                      className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600"
                      onClick={() => dispatch({ type: "card" })}
                    />
                    <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                      Master Card / Paypal / VISA Card
                    </label>
                  </div>
                </form>
              </div>

              <div className="flex mt-16 justify-between ml-16 mr-16">
                <Link href="topupResult">
                  <button className="flex  items-center font-normal font-karla">
                    <BiChevronLeftCircle className="text-4xl text-[#6C63FF] mr-4" />
                    Topup
                  </button>
                </Link>
                <button className="flex  items-center font-normal font-karla">
                  Continue
                  <BiChevronRightCircle className="text-4xl text-[#6C63FF] ml-4" />
                </button>
              </div>
            </div>
          )}
          {mode.momo && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="absolute top-14 ml-[14vw]"
              />
              <h1 className="font-bold text-xl pt-14 text-center font-karla">
                Momo payment
              </h1>
              <form action="" className="ml-16 mt-14">
                <input
                  type="text"
                  name="nameeofagent"
                  placeholder="Your phone number"
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 font-karla"
                />
                <input
                  type="text"
                  name="mobilrnumber"
                  placeholder="Amount(Frw)"
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-12 font-karla"
                />
              </form>
              <div className="flex mt-20 justify-between ml-16 mr-20">
                <Link href="methodofPayment">
                  <button className="flex  items-center font-normal font-karla">
                    <BiChevronLeftCircle className="text-4xl text-[#6C63FF] mr-4" />
                    Topup
                  </button>
                </Link>

                <button
                  onClick={() => dispatch({ type: "complete" })}
                  className="bg-[#6C63FF] text-white w-16 rounded-lg font-karla"
                  type="submit"
                >
                  Done
                </button>
              </div>
            </div>
          )}
          {mode.bank && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="absolute top-14 ml-[14vw]"
              />
              <h1 className="font-bold text-xl pt-14 text-center font-karla">
                By Bank
              </h1>
              <form action="" className=" mt-10">
                <div className="flex items-center justify-center w-full">
                  <label className="flex flex-col items-center justify-center w-2/3 h-48 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                    {/* <div className="flex flex-col items-center justify-center pt-5 pb-6 font-karlaaa">
                      <Upload className="text-3xl" />
                      <p className="mb-2 text-sm text-gray-500 dark:text-gray-400 pt-4 font-karla">
                        Upload proof of payment
                      </p>
                    </div> */}
                    <input
                      id="dropzone-file"
                      type="file"
                      //  className="hidden"
                      onChange={(e) => {
                        const target = e.target as HTMLInputElement;
                        setFiles((target!.files || null) as File[] | null);
                      }}
                    />
                  </label>
                </div>
              </form>
              <div className="flex mt-20 justify-between ml-16 mr-20">
                <Link href="methodofPayment">
                  <button className="flex  items-center font-normal font-karla">
                    <BiChevronLeftCircle className="text-4xl text-[#6C63FF] mr-4" />
                    Topup
                  </button>
                </Link>
                <button
                  onClick={() => dispatch({ type: "complete" })}
                  className="bg-[#6C63FF] text-white w-16 rounded-lg font-karla "
                >
                  Done
                </button>
              </div>
            </div>
          )}
          {mode.card && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/topupTick.svg"
                alt="topup-tick"
                className="absolute top-14 ml-[14vw]"
              />
              <h1 className="font-bold text-xl pt-14 text-center">By card</h1>
              <form action="" className="ml-16 mt-10">
                <label htmlFor="" className="font-karla">
                  CARD NUMBER
                </label>
                <input
                  type="text"
                  name="nameeofagent"
                  placeholder="Card number"
                  required
                  className=" block border-solid border bg-[#D9D9D9]  h-14 mt-2  w-5/6 rounded-lg pl-8 font-karla "
                />
                <div className="flex mt-2">
                  <div className="">
                    <label htmlFor="" className="font-karla">
                      EXPIRATION DATE
                    </label>
                    <div className="flex">
                      <input
                        type=""
                        name="mobilenumber"
                        placeholder=""
                        required
                        className=" block border-solid border bg-[#D9D9D9]  h-14 w-24 rounded-lg pl-4  mt-2 pr-4 ,font-karla"
                      />
                      <input
                        type=""
                        name="mobilenumber"
                        placeholder=""
                        required
                        className=" block border-solid border bg-[#D9D9D9]  h-14 w-24 rounded-lg ml-4  mt-2 pl-4 pr-4 font-karla"
                      />
                    </div>
                  </div>
                  <div>
                    <label htmlFor="" className="font-karla pl-6">
                      CVC
                    </label>

                    <input
                      type="text"
                      name="mobilenumber"
                      placeholder=""
                      required
                      className=" block border-solid border bg-[#D9D9D9] ml-4 h-14 w-44 rounded-lg pl-8 mt-2 "
                    />
                  </div>
                </div>
                <div className="mt-4">
                  <label htmlFor="" className="pt-4 font-karla">
                    CARD HOLDER'S NAMES
                  </label>
                  <input
                    type="text"
                    name="numberofsms"
                    placeholder="Number of SMS"
                    required
                    className=" block border-solid border bg-[#D9D9D9]  h-14  w-5/6 rounded-lg pl-8 mt-2 font-karla"
                  />
                </div>
              </form>
              <div className="flex mt-8 justify-between ml-16 mr-20">
                <Link href="methodofPayment">
                  <button className="flex  items-center font-normal font-karla">
                    <BiChevronLeftCircle className="text-4xl text-[#6C63FF] mr-4" />
                    Topup
                  </button>
                </Link>
                <button
                  onClick={() => dispatch({ type: "complete" })}
                  className="bg-[#6C63FF] text-white w-16 rounded-lg font-karla"
                >
                  Done
                </button>
              </div>
            </div>
          )}
          {mode.complete && (
            <div className="h-[70vh] w-1/3 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
              <X
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl"
                onClick={() => setUtils({ showPopup: false })}
              />
              <img
                src="/icons/sucess.svg"
                alt="topup-tick"
                className="ml-auto mr-auto pt-28"
              />
              <h1 className="text-center pt-8 text-xl">
                Sucessfully bought messages!
              </h1>
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default Topup;
