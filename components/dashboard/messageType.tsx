import React, { ReactElement, ReactEventHandler } from 'react';
import Image from 'next/image';
import { useReducer, useState } from 'react';
import { FaRegPaperPlane } from 'react-icons/fa';
import { useForm } from 'react-hook-form';
import useAuth from '../context/authContext';
import useResponse from '../context/responseContext';
import { api } from '../utils/api';
import { useRouter } from 'next/router';

interface FormValues {
  name: string;
  receiver: string;
  text: string;
  messages: number
}

type ACTIONTYPE =
  | { type: 'single' }
  | { type: 'bulk' }
  | { type: 'group' }
  | { type: 'messageIcon' }
  | { type: 'success' }
  | { type: 'failed' }
  | { type: 'typeContacts' }
  | { type: 'uploadFile' };

const initialMethod = {
  typeContacts: false,
  uploadFile: false,
};

const methodType = (method: typeof initialMethod, action: ACTIONTYPE) => {
  method = {
    typeContacts: false,
    uploadFile: false,
  };
  switch (action.type) {
    case 'typeContacts':
      return {
        ...method,
        typeContacts: true,
      };
    case 'uploadFile':
      return {
        ...method,
        uploadFile: true,
      };
    default:
      return {
        ...method,
      };
  }
};

export default function MessageType() {
  // const [recieve, dispatch] = useReducer(recieverType, initialReciever);

  const [method, setDisplayMethod] = useReducer(methodType, initialMethod);
  const { updateSuccess, updateMessage, updateFailure } = useResponse();
  const [files, setFiles] = useState<File[] | null>(null);
  const { register, handleSubmit } = useForm<FormValues>();
  const  [groupMembers, setMembers] = useState<number>(0);
  const [messageIcon,setMessageIcon] = useState<Boolean>(true)
  const router = useRouter();
  const {type } = router.query;
    const [details, updateDetails] = useReducer(
    (prev: any, next: any) => {
      return { ...prev, ...next };
    },
    { members: 0, messageChars: 0, price: 0, messages: 0 },
  );
  const { user } = useAuth();
  const submit = (data: FormValues, event: any) => {
    event.preventDefault();
    data.messages =  details.messages
    if (type == 'bulk' && method.uploadFile) {
      const formData = new FormData();
      formData.append('receivers', files![0]);
      formData.append('name', data.name);
      formData.append('messages', details.messages);
      formData.append('text', data.text);
      // (true)
      api
        .post('/message/send/multiple/file', formData, {
          headers: {
            'Content-Type': 'application/json',
          },
        })
        .then((response) => {
          updateSuccess();
          updateMessage(response.data.message);
        });
    } else {
      const url = (type == 'single') 
        ? `/message/send/single`
        : (type == 'bulk') 
        ? `/message/send/multiple`
        : `/message/send/group`;
      api
        .post(url, data)
        .then((res) => {
          updateSuccess();
          updateMessage(res.data.message);
        })
        .catch((err) => {
          updateFailure();
          updateMessage(err.message);
        });
    }
  };
  return (
    <div className="w-full font-karla">
      <div className="h-20 w-full border-b-2 border-solid flex justify-center float-right items-center text-[#6C63FF] text-xl ">
        <h1>Messages</h1>
      </div>
      
    
      { type == 'single'? type == 'single':type == 'bulk'? type == 'bulk':type=='group' ? type=='group': messageIcon && (
      
        <div className="h-96 mt-28 3xl:absolute z-0 2xl:flex 2xl:justify-center xl:flex xl:justify-center  lg:flex lg:justify-center  right-[35%] md:flex md:justify-center sm:flex sm:justify-center ">
          <Image
            width={400}
            height={400}
            alt="signup image"
            src={'/images/messages.png'}
            className="mt-12 h-[48vh]"  
          />
        </div>
      )}
      {type == 'single' && (
        <div>
          <div className="h-96 w-3/4 mt-28 3xl:absolute 2xl:absolute xl:absolute lg:absolute 3xl:right-10 2xl:right-10 xl:right-10 lg:right-10  ">
            <h1 className="text-2xl text-center font-inter ">SEND MESSAGE</h1>
            {/* <div className="bg-blue-400 w-2/3 ml-auto mr-auto"></div> */}
            <form
              action=""
              className="3xl:w-3/4 2xl:w-3/4 xl:w-3/4 lg:w-3/4 md:w-[80vw] sm:w-[80vw] md:pl-8 sm:ml-6 sm:mr-4 ml-auto mr-auto"
              onSubmit={handleSubmit(submit)}
            >
              <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20 mt-10">
                <label htmlFor="name" className="pt-4 font-karla">
                  Sender
                </label>
                <input
                  {...register('name')}
                  name="name"
                  type="text"
                  placeholder="Add sender name"
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-24 "
                />
              </div>
              <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20  mt-4">
                <label htmlFor="name" className="pt-4 font-karla">
                  Phone_number
                </label>
                <input
                  {...register('receiver')}
                  name="receiver"
                  type="telephone"
                  placeholder="Add telephone"
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-8 "
                />
              </div>

              <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-24 lg:ml-20  mt-4 font-inter">
                <label htmlFor="Message " className='font-karla'>Message</label>
                <textarea
                  {...register('text')}
                  name="text"
                  placeholder="Type a message.."
                  onChange={(e) => {
                    updateDetails({ messageChars: e.target.value.length });
                    updateDetails({
                      messages: Math.floor(details.messageChars / 160) + 1,
                    });
                  }}
                  className=" block border-solid border pt-4 h-48 max-h-48 min-h-full border-[#6C63FF] border-opacity-10 3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-20 "
                ></textarea>
              </div>
              <div className="3xl:flex 2xl:flex xl:flex lg:flex md:flex gap-8 mt-8 3xl:ml-24 2xl:ml-24 xl:ml-24 lg:ml-24 z-0">
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messageChars}
                  </p>
                  <p>Characters</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages}
                  </p>
                  <p>Total Messages</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages * user?.buying_price!}
                  </p>
                  <p>Frw</p>
                </div>
              </div>
              <button
                type="submit"
                className=" bg-blue-600 text-white rounded-lg flex h-12 items-center w-28 pl-6 float-right mt-8 3xl:mr-36 2xl:mr-36 xl:mr-36 lg:mr-36 md:mr-40 font-inter"
              >
                <FaRegPaperPlane className="mr-3 " />
                SEND
              </button>
            </form>
          </div>
        </div>
      )}

      {type == 'bulk' && (
        <div>
          <div className="h-96 w-3/4 mt-28 3xl:absolute 2xl:absolute xl:absolute lg:absolute 3xl:right-10 2xl:right-10 xl:right-10 lg:right-10  md:pl-8">
            <h1 className="text-2xl text-center font-inter">SEND MESSAGE</h1>

            <form
              action=""
              onSubmit={handleSubmit(submit)}
              className="3xl:w-3/4 2xl:w-3/4 xl:w-3/4 lg:w-3/4 md:w-[80vw] sm:w-[80vw] md:pl-8 sm:ml-6 sm:mr-4 ml-auto mr-auto"
            >
              <div className="flex 3xl:ml-36 2xl:ml-36 xl:ml-36 lg:ml-36  mt-10">
                <label htmlFor="name" className="pt-4 font-karla">
                  Sender
                </label>
                <input
                  {...register('name')}
                  name="name"
                  type="text"
                  placeholder="sender displayed name"
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-24 "
                />
              </div>
              <div className="lg:flex sm:block">
                <div className="flex mt-6">
                  <label
                    htmlFor="name"
                    className="3xl:ml-36 2xl:ml-36 xl:ml-36 lg:ml-36 font-karla"
                  >
                    Method_type
                  </label>

                  <div className="flex items-center">
                    <input
                      onClick={() => setDisplayMethod({ type: 'typeContacts' })}
                      id="default-radio-2"
                      type="radio"
                      value=""
                      placeholder="Enter the receivers number separated with comma"
                      name="default-radio"
                      className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600 pl-8 ml-12"
                    />
                    <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                      Type contacts
                    </label>
                  </div>
                  <div className="flex items-center">
                    <input
                      onClick={() => setDisplayMethod({ type: 'uploadFile' })}
                      id="default-radio-2"
                      type="radio"
                      value="uploadFile"
                      name="default-radio"
                      className="w-6 h-6 text-blue-600 bg-gray-100 border-gray-300  dark:bg-gray-700 dark:border-gray-600 3xl:ml-40 2xl:ml-40 xl:ml-28 lg:ml-28 md:ml-6 sm:ml-6"
                    />
                    <label className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300 font-karla">
                      Upload a file
                    </label>
                  </div>
                </div>
              </div>

              <div className="block">
                {method.typeContacts && (
                  <textarea
                    className="h-24  3xl:ml-[32%] 2xl:ml-[34%] xl:ml-[32%] lg:ml-[32%] md:ml-[31%] sm:ml-[45%] mt-4 w-1/2 min-h-24 max-h-24 text-blue-600 bg-gray-100 bg-opacity-20 border-gray-300  dark:bg-gray-700 dark:border-gray-600 pl-8 pt-4 rounded-lg text-sm font-inter"
                    placeholder="Type telephone numbers of receivers separated with comma"
                    {...register('receiver')}
                    name="receiver"
                  ></textarea>
                )}
                {method.uploadFile && (
                  <div className="flex mt-4 w-full">
                    <label className="flex flex-col items-center justify-center w-1/2 sm:w-[45vw] 3xl:ml-[32%] 2xl:ml-[32%] xl:ml-[32%] lg:ml-[32%] md:ml-[36%] sm:ml-[45%] h-24 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600 font-inter">
                      <input
                        type="file"
                        name="receiver"
                        className="ml-32 md:ml-20 sm:ml-24 sm:block  md:text-sm sm:text-xs"
                        accept={'.csv'}
                        onChange={(e) => {
                          const target = e.target as HTMLInputElement;
                          setFiles((target!.files || null) as File[] | null);
                        }}
                      />
                    </label>
                  </div>
                )}
              </div>
              <div className="flex 3xl:ml-36 2xl:ml-36 xl:ml-36 lg:ml-36 mt-4">
                <label htmlFor="Message" className="font-karla">
                  Message
                </label>
                <textarea
                  {...register('text')}
                  name="text"
                  placeholder="Type a message.."
                  onChange={(e) => {
                    updateDetails({ messageChars: e.target.value.length });
                    updateDetails({
                      messages: Math.floor(details.messageChars / 160) + 1,
                    });
                  }}
                  className=" block border-solid border pt-4 h-48 max-h-48 min-h-full border-[#6C63FF] border-opacity-10 3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-24 "
                ></textarea>
              </div>
              <div className="3xl:flex 2xl:flex xl:flex lg:flex md:flex gap-8 mt-8 3xl:ml-24 2xl:ml-24 xl:ml-24 lg:ml-24">
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messageChars}
                  </p>
                  <p>Characters</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages}
                  </p>
                  <p>Total Messages</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages * user?.buying_price!}
                  </p>
                  <p>Frw</p>
                </div>
              </div>
              <button
                type="submit"
                className=" bg-blue-600 text-white rounded-lg flex h-12 items-center w-28 pl-6 float-right mt-8 mr-36 font-inter"
              >
                <FaRegPaperPlane className="mr-3" />
                SEND
              </button>
            </form>
          </div>
        </div>
      )}

      {type == 'group' && (
        <div>
          <div className="h-96 w-3/4 mt-28 3xl:absolute 2xl:absolute xl:absolute lg:absolute 3xl:right-10 2xl:right-10 xl:right-10 lg:right-10  ">
            <h1 className="text-2xl text-center font-inter">SEND MESSAGE</h1>

            <form
              action=""
              onSubmit={handleSubmit(submit)}
              className="3xl:w-3/4 2xl:w-3/4 xl:w-3/4 lg:w-3/4 md:w-[80vw] sm:w-[80vw] md:pl-8 sm:ml-6 sm:mr-4 ml-auto mr-auto"
            >
               <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20  mt-4">
                <label htmlFor="name" className="pt-4 font-karla">
                  Sender
                </label>
                <input
                  {...register('name')}
                  name="name"
                  type="text"
                  placeholder="Add name"
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-14"
                />
              </div>
              <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20  mt-4">
                <label htmlFor="name" className="pt-4 font-karla">
                  Receiver 
                </label>
                <select
                  {...register('receiver')}
                  name="receiver"
                  placeholder="Group name"
                  id="group"
                  onChange={(e) => {
                    const group = user!.groups.filter(
                      (item) => item.id == e.target.value,
                    );
                    updateDetails({members: group[0].members });
                  }}
                  className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-12 "
                >
                  <option value="">Group Name</option>
                  {user!.groups.map((group) => {
                    return <option value={group.id}>{group.name}</option>;
                  })}
                </select>
              </div>
              <div className="flex items-center gap-4 justify-center mt-4">
                <p className="h-12 w-12 border drop-shadow border-[#6C63FF] rounded-full flex justify-center items-center">
                  {details.members}
                </p>
                <p>Members</p>
              </div>
              <div className="flex 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20  mt-4">
                <label htmlFor="Message" className="font-karla">
                  Message 
                </label>
                <textarea
                  {...register('text')}
                  name="text"
                  onChange={(e) => {
                    updateDetails({ messageChars: e.target.value.length });
                    updateDetails({
                      messages: Math.floor(details.messageChars / 160) + 1,
                    });
                  }}
                  id=""
                  placeholder="Type a message.."
                  className=" block border-solid border pt-4 h-48 max-h-48 min-h-full border-[#6C63FF] border-opacity-10 3xl:w-2/3 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-[60vw] sm:w-[70vw] rounded-lg pl-8 ml-12 "
                ></textarea>
              </div>
              <div className="3xl:flex 2xl:flex xl:flex lg:flex md:flex gap-8 mt-8 3xl:ml-24 2xl:ml-24 xl:ml-24 lg:ml-24">
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messageChars}
                  </p>
                  <p>Characters</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages}
                  </p>
                  <p>Total Messages</p>
                </div>
                <div className="flex items-center gap-4 ">
                  <p className="h-12 w-12 border drop-shadow rounded-full flex justify-center items-center">
                    {details.messages * user?.buying_price!}
                  </p>
                  <p>Frw</p>
                </div>
              </div>
              <button
                type="submit"
                className=" bg-blue-600 text-white rounded-lg flex h-12 items-center w-28 pl-6 float-right mt-8 mr-36 font-inter"
              >
                <FaRegPaperPlane className="mr-3" />
                SEND
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}
