import React from 'react'
import Image from "next/image";
import { useState,useEffect} from "react";
import { X, Search, XCircle, Bell, Gear } from "react-bootstrap-icons";
import router from "next/router"
import useAuth from '../context/authContext';
import Link from "next/link";
import { api } from '../utils/api';
import { admin, agent, reseller, superReseller } from "./menu";
import { FiLogOut } from 'react-icons/fi';

function NewSideBar() {
    const [deleteAccount, setDeleteAccount] = useState<Boolean>(false);
    const [settings, setSettings] = useState<Boolean>(false);
    const [password, setPassword] = useState<any>(null);
    const [loggingOut, setLoggingOut] = useState<Boolean>(false);
    const { logout,user } = useAuth();
    const logOut = () => {
        logout();
      };
      const url = `/agent/delete/${user?.id}`;
      const deletingAccount = async () => {
        const data: any = { password };
        await api.post(url, data).then((res) => {
          logout();
          router.push('/auth/login');
        });
      };
      const menu =
    user?.position_id == 1
      ? admin
      : user?.position_id == 2
      ? superReseller
      : user?.position_id == 3
      ? reseller
      : agent;
  useEffect(() => {
    window.onclick = function (event: any) {
      if (!event.target.matches(".dropbtn")) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (!openDropdown.classList.contains("hidden")) {
            openDropdown.classList.add("hidden");
          }
        }
      }
    };
  });
  return (
    // <div className="">
     
        <div className="h-screen w-full bg-white absolute z-50 shadow-md shadow-indigo-500/40 pl-8">
          <div className="flex h-14">
            <Image
              height={300}
              width={25}
              src={"/images/profile.jpg"}
              alt="profile"
              className="rounded-full h-6 mt-4"
            />
            <p className="font-karla pl-2 pt-5 text-sm">Profile</p>
          </div>
          <form className="flex items-center ">
            <label className="sr-only">Search</label>
            <div className="relative w-full">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <Search className="text-[#6C63FF]" />
              </div>
              <input
                type="text"
                id="simple-search"
                className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                placeholder="Search"
                required
              />
            </div>
            <button
              type="submit"
              className="p-2.5 ml-2 mr-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              <Search />
              <span className="sr-only">Search</span>
            </button>
          </form>

          {menu.map((item) => {
            return (
              <div>
                <div
                  id="dropdownDelayButton"
                  onClick={() => {
                    document
                      .getElementById(item.id)
                      ?.classList.remove("hidden");
                  }}
                  className="dropbtn flex  mt-8 hover:text-[#6C63FF] hover:border-r-4 hover:border-solid hover:border-[#6C63FF]"
                >
                  {item.icon}
                  <p className="font-karla pl-2 text-sm pt-1">{item.link}</p>
                </div>
                <div
                  id={item.id}
                  className="dropdown-content z-50 hidden absolute left-52 bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700"
                >
                  <ul
                    className="py-2 text-sm text-gray-700 dark:text-gray-200"
                    aria-labelledby="dropdownDelayButton"
                  >
                    {item.nestedLinks.map((item) => {
                      return (
                        <Link href={item.href}>
                          <p className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                            {item.link}
                          </p>
                        </Link>
                      );
                    })}
                  </ul>
                </div>
              </div>
            );
          })}
          <div className="flex w-40 h-10 mt-8">
            <Bell
            //   onClick={async () => {
            //     await api.get("/notifications/get").then((response) => {
            //       setList(response.data.notifications);
            //     });
            //     setNotification(true);
            //   }}
              className=" text-xl"
            />
            <p className="font-karla pl-2 te xt-sm">Notifications</p>
          </div>
          <div className="mt-40 pb-8">
            {/* <Link href={{pathname:"dashboard/updateUser?type=settings"}}> */}
            <button
              onClick={()=>setSettings(true)}
              className="flex w-full  hover:text-[#6C63FF] hover:border-r-4 hover:border-solid   hover:border-[#6C63FF]"
            >
              <Gear className="h-6  rounded-l-lg text-sm font-light" />
              <p className="font-karla pl-2">Settings</p>
            </button>
            {/* </Link> */}
            <button
              onClick={() => setLoggingOut(true)}
              className="flex w-full  mt-6  hover:text-[#6C63FF] hover:border-r-4 hover:border-solid   hover:border-[#6C63FF]"
            >
              <FiLogOut className=" rounded-l-lg text-sm font-light h-6" />
              <p className="font-karla pl-2">Logout</p>
            </button>
          </div>
          {settings && (
            <div className="h-32 w-48 rounded-xl bg-white z-50 absolute left-52 bottom-[20%] shadow-md shadow-gray-200">
              <div className="block">

                  <Link href={{pathname:"updateUser", query:{type:"editProfile"}}}>
                <button className="h-10 w-full mt-6 text-sm font-inter hover:bg-gray-100">
                  Edit profile
                </button>
                  </Link>
                <button
                  onClick={() => setDeleteAccount(true)}
                  className="h-10 w-full text-sm font-inter hover:bg-gray-100"
                >
                  Delete Account
                </button>
              </div>
            </div>
          )}
          
             {deleteAccount && (
          <div className="h-screen w-full ml-52 bg-gray-800 absolute z-0 right-0 top-0 opacity-90 z-50">
            <div className="h-auto pb-[4%] w-[70vw] bg-white ml-auto mr-auto mt-[10%] backdrop:blur-3xl">
              <XCircle
            
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
                onClick={() => setDeleteAccount(false)}
              />
              <h1 className="text-2xl font-bold font-karla text-center pt-6">
                Delete my Account
              </h1>
              <p className=" text-center font-karla pl-20 pr-20 pt-6 pb-6">
                Please enter the password to confirm your account deletion
              </p>
              <form action="">
                <input
                  onChange={(e: any) => setPassword(e.target.value)}
                  type="password"
                  placeholder="Password"
                  className=" block border-solid border border-[#6C63FF] border-opacity-50 h-14  w-3/4 rounded-lg pl-8 font-inter ml-12 mt-3"
                />
                <div className="flex justify-center">
                  <button
                    onClick={() => deletingAccount()}
                    type="button"
                    className="h-14 w-40 text-white bg-[#6C63FF] rounded-lg font-karla mt-4"
                  >
                    Delete Account
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
        
               {loggingOut &&(
            <div className="w-full absolute top-0 bottom-0 left-0 right-0 bg-gray-800 opacity-90">
              <div className="h-40 w-96 bg-white mr-auto ml-20 mt-[36%] pt-10">
                <h4 className="text-lg text-center font-karla font-semibold">
                  Are you sure you want to logout?
                </h4>
                <div className="flex justify-evenly mt-8">
                  <button
                    onClick={()=> setLoggingOut(false)}
                    className="h-10 w-20 rounded-lg border-2 border-solid border-blue-500"
                  >
                    No
                  </button>
                  <button
                    onClick={() => logOut()}
                    className="h-10 w-20 rounded-lg border-2 border-solid border-blue-500"
                  >
                    Yes
                  </button>
                </div>
              </div>
            </div>
          )}
         
        </div>
    // </div>
  )
}

export default NewSideBar