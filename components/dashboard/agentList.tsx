import React, { useEffect, useReducer, useState } from 'react';
import { CircleFill, Search } from 'react-bootstrap-icons';
import { useRouter } from 'next/router';
import { api } from '../utils/api';
import useResponse from '../context/responseContext';
import useAuth from '../context/authContext';
import SpinnerLoader from '../spinnerLoader/spinnerLoader';
import Paginator from '../utils/paginator';
import useLoader from '../context/loaderContext';
import useStateCallback from '../utils/useStateCallback';

type Agent = {
  [x: string]: any;
  name: string;
  phone_number: string;
  email: string;
  position_id: number;
  status: string;
};
interface Dates {

}
function AgentList(): JSX.Element {
  const [agents, setAgents] = useStateCallback<Agent[]>([]);
  const { updateSuccess, updateMessage } = useResponse();
  const [current, setCurrent] = useStateCallback<Agent[]>([]);

  const { user } = useAuth();
  const {loading, updateLoading} = useLoader();
  const [query,setQuery] = useState('')
  const [searchedVal,setSearchedVal] = useState("")
  const getAgents = () => {
    updateLoading(true);
    api
      .get(`/agent/all`, {
        headers: {
          'Accept-Encoding': 'application/json',
        },
      })
      .then((response) => {
        setAgents(response.data.agents,(agents:any)=>{
          setCurrent(agents!.slice(0,5));
        })
        updateLoading(false);
      });
  };
  useEffect(getAgents, []);
  const router = useRouter();
  const {type } = router.query;
  return (
    <div className="w-full">
      <div className="h-20 w-full border-b-2 border-solid flex justify-center float-right items-center font-karla text-[#6C63FF]  text-xl">
        <h1>Agents List</h1>
      </div>
      {type == "agentList" && (
        <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[77vw] lg:w-[74vw] mt-20 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
             <form className="flex items-center 3xl:w-1/4 2xl:w-1/4 xl:2/3 mt-4 lg:ml-6 md:ml-6 sm:ml-6 lg:mr-4 md:mr-4 sm:mr-4">
          <label className="sr-only">Search</label>
          <div className="relative w-full">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <Search className="text-[#6C63FF]" />
            </div>
            <input
              type="text"
              id="simple-search"
              className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
              placeholder="Search"
              required
              onChange={(e)=>setSearchedVal(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            <Search />
            <span className="sr-only">Search</span>
          </button>
        </form>
        <div className="flex flex-col mt-4">
          <div className="overflow-x-auto">
            <div className="p-1.5 w-full inline-block ">
              <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100  overflow-x-auto overflow-hidden">
                <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                  <thead className="bg-gray-50 font-inter">
                    <tr>
                      <th scope="col" className="px-6 py-3  text-left ">
                        Agent name
                      </th>
                      <th
                        // scope="col"
                        className=" py-3 placeholder:text-left  "
                      >
                        Mobile Number
                      </th>
                      <th scope="col" className="px-6 py-3  text-left  ">
                        Email
                      </th>
                      <th scope="col" className="px-6 py-3 text-left  ">
                        Category
                      </th>
                      <th scope="col" className="px-6 py-3  text-left">
                        Account Status
                      </th>
                      <th scope="col" className="px-6 py-3 text-left  ">
                        Buying price
                      </th>
                      <th scope="col" className="px-6 py-3 text-left  ">
                        Balance
                      </th>
                      <th scope="col" className="px-6 py-3 text-left  ">
                        Total Transacted
                      </th>
                      {user?.position_id! <= 2 ? (
                        <th scope="col" className="px-6 py-3 text-left  ">
                          Supplier
                        </th>
                      ) : (
                        ''
                      )}
                      <th scope="col" className=" py-3 px-6  text-left ">
                        Actions
                      </th>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200 font-inter">
                    {current
                        .filter((row: Agent) => {
                          if (!searchedVal.length) {
                            return true; // return all rows if search value is not provided
                          }
                          const name = row.name
                            ? row.name.toString().toLowerCase()
                            : ""; // convert name to string and lowercase it
                          const searched = searchedVal.toString().toLowerCase(); // convert search value to string and lowercase it
                          return name.includes(searched); // return true if name includes the search value
                        })
                    .map((agent_data: Agent) => (
                      <tr>
                        <td className="px-6 py-5 text-sm font-medium text-gray-800 whitespace-nowrap">
                          {agent_data.name}
                        </td>
                        <td className="px-12 py-5 text-sm text-gray-800 whitespace-nowrap flex">
                          {agent_data.phone_number}
                        </td>
                        <td className="px-8 py-5 text-sm text-gray-800 whitespace-nowrap">
                          {agent_data.email}
                        </td>
                        <td className="py-5 px-6 text-sm font-medium text-left whitespace-nowrap">
                          {agent_data.position_id === 1
                            ? 'ADMIN'
                            : agent_data.position_id === 2
                            ? 'SUPER RESELLER'
                            : agent_data.position_id === 3
                            ? 'RESELLER'
                            : 'AGENT'}{' '}
                        </td>
                        <td className="px-6  text-sm font-medium text-left justify-middle whitespace-nowrap flex">
                          {agent_data.status}
                          {agent_data.status === 'ACTIVE' ? (
                            <CircleFill className="text-green-500 ml-4 text-xs" />
                          ) : (
                            <CircleFill className="text-red-500 ml-4 text-xs" />
                          )}
                        </td>
                        <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                          {agent_data.buying_price}
                        </td>
                        <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                          {agent_data.total_sms}
                        </td>
                        <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                          {agent_data.total_transacted}
                        </td>
                        {user?.position_id! <= 2 ? (
                          <td className="px-6  text-sm font-medium text-left whitespace-nowrap">
                            {agent_data.created_by_name}
                          </td>
                        ) : (
                          ''
                        )}

                        <td className="px-6">
                          <div className="flex">
                            <label className="inline-flex relative cursor-pointer">
                              <input
                                onClick={() => {
                                  updateLoading(true)
                                  api
                                    .put(
                                      `/agent/account/${
                                        agent_data.status == 'ACTIVE'
                                          ? 'INACTIVE'
                                          : 'ACTIVE'
                                      }/${agent_data.id}`,
                                    )
                                    .then((res) => {
                                      updateSuccess();
                                      updateLoading(false)
                                      updateMessage(res.data.message);
                                      getAgents();
                                    })
                                    .then((err) => {
                                      console.log(err);
                                    });
                                }}
                                type="checkbox"
                                value=""
                                className="sr-only peer"
                                checked={
                                  agent_data.status == 'ACTIVE' ? false : true
                                }
                              />
                              {loading ?  (
                                <SpinnerLoader/>
                                ) :  (
                                <div className="w-10 h-5 bg-green-500 rounded-full peer peer-focus:ring-4 peer-focus:ring-white dark:peer-focus:ring-green-800 dark:bg-green-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-red-600"></div>
                              )}
                            </label>
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="ml-auto mr-auto">
            {loading ? <SpinnerLoader /> : null}
          </div>
        </div>
        {/* <div className="flex justify-center mt-2">
          <BiChevronLeftCircle className="text-3xl text-[#6C63FF]" />
          <BiChevronRightCircle className="text-3xl text-[#6C63FF]" />
        </div> */}
        <Paginator numPages={6} data={agents} setCurrent={setCurrent} current={current} />
      </div>
      )}
    </div>
  );
}
export default AgentList;
