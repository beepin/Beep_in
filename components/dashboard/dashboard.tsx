import React, { useState, useEffect, useMemo } from "react";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import { getCookie } from "cookies-next";
import { api } from "../utils/api";
import { exportTableToCSV } from "../utils/download";
import { useRouter } from "next/router";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import {
  CloudDownload,
  Send,
  ChatRightDots,
  PersonCircle,
  Folder,
  Search,
} from "react-bootstrap-icons";
import "chart.js/auto";
import { PieChart, Pie } from "recharts";


type Report = {
  name: string;
  created_at: string;
  text: string;
  receiver: string;
  status: string;
};

interface Data {
  date: string;
}
import { People } from "react-bootstrap-icons";
import { AiOutlineRight } from "react-icons/ai";
import { useReducer, useRef } from "react";
import Paginator from "../utils/paginator";
import useResponse from "../context/responseContext";
type ACTIONTYPE = { type: "sent" } | { type: "recieved" };

const initialStatics = {
  sent: true,
  recieved: true,
};
const eventChange = (view: typeof initialStatics, action: ACTIONTYPE) => {
  view = {
    sent: false,
    recieved: false,
  };
  switch (action.type) {
    case "sent":
      return {
        ...view,
        sent: true,
      };

    case "recieved":
      return {
        ...view,
        recieved: true,
      };
    default:
      return {
        ...view,
      };
  }
};

const fetchMessages = async () => {
  const messages = await api.get("message/get/groupMessages").then((data) => console.log("Here are the data: ", data, " data received"));
  return messages;
};

function Dashboard(): JSX.Element {
  const [state, dispatch] = useReducer(eventChange, initialStatics);
  const [overview, setOverview] = useReducer(
    (prev: any, next: any) => {
      return { ...prev, ...next };
    },
    {
      top_up: 0,
      balance: 0,
      agents: 0,
      groups: 0,
      sent: 0,
      pending: 0,
      messages: 0,
      failed: 0,
      agentsList: [],
      groupsList: [],
    }
  ); 
  const [messagingStatus, setMessagingStatus] = useState<any[]>([]);
  const [sms, setDisplaySms] = useState<Boolean>(false);
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState<any[]>([]);
  const [bulkReport, setBulk] = useState<Report[]>([]);
  const [current, setCurrent] = useState<Report[]>([]);
  const { updateSuccess, updateMessage, updateFailure } = useResponse();
  const [searchedVal, setSearchedVal] = useState("");
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  const [topupHistory, setTopupHistory] = useState<any[]>([])
  const fetchMessages = async () => {
    const response = await api.get("/message/get/groupMessages");
    setMessagingStatus(response.data.messagingStatus);
    console.log("Here are the data: ", response.data);
    return response.data;
  };
  
  const fetchTopups = async () => {
    const response = await api.get("/topup/chart/grouped");
    setTopupHistory(response.data.topupHistory );
    console.log("Here is the data about topup: ", response.data);
    return response.data;
  };
  
  useEffect(() => {
    fetchMessages();
    fetchTopups();
  }, []);
  const data = messagingStatus?.map((data) => ({
    label: data.label,
    sent: data.sent,
    received: data.received,
    failed: data.failed
  }));

  
  const details = topupHistory?.map((data) => ([
      {
        name: "Accepted topup requests",
        students: 400,
        fill: "#8a000080",
        label: (data.approved / (data.approved+data.requested+data.failed) * 100)
      },
      {
        name: "Pending topup requests",
        students: 400,
        fill: "#E5DAFB",
        label: (data.requested / (data.approved+data.requested+data.failed) * 100)
      },
      {
        name: "Denied topup requests ",
        students: 600,
        fill: "#4caf5080",
        label: (data.failed / (data.approved+data.requested+data.failed) * 100)
      },
    ]));
  const handleStartDateChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setStartDate(new Date(event.target.value));
  };

  const handleEndDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEndDate(new Date(event.target.value));
  };
  const resend = (data: any) => {
    const url =
      router.query.type == "bulkReport"
        ? `/message/send/multiple`
        : `/message/send/single`;
    let data1 = {
      messages: Math.floor(data.text.length / 10),
      text: data.text,
      receiver:
        router.query.type == "bulkReport"
          ? data.recipient.join(",")
          : data.receiver,
      name: data.name,
    };
    api
      .post(url, data1)
      .then((res) => {
        updateSuccess();
        updateMessage(res.data.message);
      })
      .catch((err) => {
        updateFailure();
        updateMessage(err.message);
        console.log(err);
      });
    getMessages();
  };

  const getOverview = async () => {
    const token = getCookie('accessToken');
    await api.get("agent/overview").then((res) => {
      setOverview({
        top_up: res.data.data.top_up,
        balance: res.data.data.balance,
        agents: res.data.data.agents,
        groups: res.data.data.groups,
        sent: res.data.data.sent,
        pending: res.data.data.pending,
        failed: res.data.data.failed,
        messages: res.data.data.messages,
        groupsList: res.data.data.groupNames,
      });
    });
    await api.get("/agent/all").then((res) => {
      setOverview({ agentsList: res.data.agents });
    });
  };
  const messageType = [
    { value: "Message Type", label: "Message Type" },
    { value: "Single", label: "Single" },
    { value: "Bulk", label: "Bulk" },
    { value: "Group", label: "Group" },
  ];
  const messageStatus = [
    { value: "Message Status", label: "Message Status" },
    { value: "Sent", label: "Sent" },
    { value: "Recieved", label: "Recieved" },
    { value: "Failed", label: "Failed" },
  ];
  const getMessages = () => {
    api.get(`/message/bulk/get`).then((response) => {
      setBulk(response.data.result);
      setIsLoading(false);
    });
    api.get(`/message/get`).then((response) => {
      // setMessage(response.data.result);*
      setIsLoading(false);
      if (state.sent) {
        const sent = response.data.result.filter((item: any) => {
          return item.status == "sent";
        });
        setMessage(sent);
      } else if (state.recieved) {
        const received = response.data.result.filter((item: any) => {
          return item.status == "recieved";
        });
        setMessage(received);
      } else {
        setMessage(response.data.result);
      }
      if (startDate && endDate) {
        const filtered = response.data.result.filter((item: any) => {
          const date = new Date(item.createdAt);
          return date >= startDate && date <= endDate;
        });
        setMessage(filtered);
      }
    });
  };

  const getChartData = ()=>{
    api.get('/topup/chart/grouped').then((response:any)=>{
      console.log(response)
    })
  }

  useEffect(() => {
    getChartData();

    setIsLoading(true);

    getMessages();

    getOverview();
  }, []);

  const [displayingReport, setDisplayingReport] = useState<Boolean>(true);

  const router = useRouter();
  const { type } = router.query;

  return (
    <div className="w-full">
      <div className="h-14 w-full border-b-2  border-solid flex justify-center float-right items-center font-karla">
        <h1 className="text-[#6C63FF] text-lg">Analytics</h1>
      </div>
      {type == "overview" && (
        <div className="h-screen w-full 3xl:flex 2xl:flex xl:flex lg:block md:block sm:block justify-evenly bg-gray-100 mt-14 pt-8 overflow-y-auto">
          <div className="block 3xl:w-1/3 2xl:w-1/3 xl:w-1/3 lg:w-full md">
            <div className="ml-8 3xl:flex 2xl:flex xl:block lg:flex md:flex sm:flex lg:w-11/12 md:w-11/12 sm:w-11/12 bg-white h-auto pt-6 pb-6 rounded-lg">
              <div className="w-1/2 xl:h-40 ">
                <ResponsiveContainer width="100%" height={300}>
                  <PieChart>
                    <Pie
                      data={details}
                      dataKey="students"
                      // outerRadius={100}
                    />
                  </PieChart>
                </ResponsiveContainer>
              </div>

              <div className="block mt-28 2xl:mt-28 ml-4">
                <div className="flex">
                  <div className="h-4 w-4 bg-[#4caf5080] rounded-full mt-1"></div>
                  <p className="pl-2 pt-1 text-xs font-inter text-black text-opacity-50">
                    Accepted topup requests
                  </p>
                </div>
                <div className="flex">
                  <div className="h-4 w-4 bg-[#E5DAFB] rounded-full mt-1"></div>
                  <p className="pl-2 pt-1 text-xs font-inter text-black text-opacity-50">
                    Pending topup requests
                  </p>
                </div>
                <div className="flex">
                  <div className="h-4 w-4 bg-[#8a000080] rounded-full mt-1"></div>
                  <p className="pl-2 pt-1 text-xs font-inter text-black text-opacity-50">
                    Denied topup requests{" "}
                  </p>
                </div>
              </div>
            </div>
            <div className="bg-white w-auto lg:w-11/12 md:w-11/12 sm:w-11/12 ml-8 p mt-4 pt-8 pb-4 rounded-lg">
              <div className="flex justify-center">
                <div className="h-9 w-9 bg-slate-200 rounded-lg text-center items-center flex xl:ml-2 ">
                  <People className="text-[#6C63FF] text-xl text-center ml-auto mr-auto " />
                </div>
                <div className="pl-8 lg:pr-14 md:pr-14 sm:pr-14">
                  <p className="text-xs font-inter text-black text-opacity-50">
                    Your groups
                  </p>
                  <p>{overview.groups}</p>
                </div>
                <div className="h-9 w-9 bg-slate-200 rounded-lg text-center items-center 3xl:ml-8 2xl:ml-6 xl:ml-1 flex">
                  <Folder className="text-green-500 text-xl text-center ml-auto mr-auto" />
                </div>
                <div className="pl-8 xl:pr-2">
                  <p className="text-xs font-inter text-black text-opacity-50">
                    Balance
                  </p>
                  <p>{overview.balance}</p>
                </div>
              </div>
              <div className="flex justify-center mt-4">
                <div className="h-9 w-9 bg-slate-200 rounded-lg text-center items-center xl:ml-2 flex">
                  <PersonCircle className="text-green-500 text-xl text-center ml-auto mr-auto" />
                </div>
                <div className="pl-8 lg:pr-14 md:pr-14 sm:pr-14">
                  <p className="text-xs font-inter text-black text-opacity-50">
                    Your agents
                  </p>
                  <p>{overview.agents}</p>
                </div>
                <div className="h-9 w-9 bg-slate-200 rounded-lg text-center items-center flex 3xl:ml-8 2xl:ml-6 xl:ml-1">
                  <ChatRightDots className="text-[#6C63FF] text-xl text-center ml-auto mr-auto" />
                </div>
                <div className="pl-8 xl:pr-2">
                  <p className="text-xs font-inter text-black text-opacity-50">
                    Messages
                  </p>
                  <p>{overview.messages}</p>
                </div>
              </div>
            </div>
            <div className="mt-4 bg-white pb-6 pl-10 rounded-lg pt-8 ml-8 text-sm lg:w-11/12 md:w-11/12 sm:w-11/12">
              <h1 className="font-bold font-inter">Your cards</h1>
              <div className="h-12 w-56 xl:w-48 bg-white shadow-lg shadow-grey-200 mt-4 rounded-xl pt-2 pl-4 flex">
                <img
                  src="/images/masterCard.png"
                  alt="masterCard"
                  className="h-8 w-10 "
                />
                <p className="pt-2 pl-2 font-karla">Master card</p>
              </div>
              <div className="h-12 w-56 xl:w-48 bg-white shadow-lg shadow-grey-200 mt-4 rounded-xl pl-4 pt-2 flex">
                <img
                  src="/images/payPal.png"
                  alt="masterCard"
                  className="h-8 w-10 "
                />
                <p className="pt-2 pl-3 font-karla">Paypal</p>
              </div>
              <div className="h-12 w-56 xl:w-48 bg-white shadow-lg shadow-grey-200 mt-4 rounded-xl pt-2 pl-4 flex">
                <img
                  src="/images/visa.png"
                  alt="masterCard"
                  className="h-8 w-10 "
                />
                <p className="pt-2 pl-3 font-karla">Visa</p>
              </div>
            </div>
          </div>

          <div className="block lg:mt-28 w-1/3 lg:w-full md:w-full sm:w-full ml-8 md:mt-28 sm:mt-28">
            <div className="bg-white  rounded-lg pl-14 pt-6 pb-6 lg:w-11/12 md:w12-11/ sm:w-11/12">
              <table className=" h-36 w-11/12 font-inter text-sm">
                <tr className="border-b-2">
                  <th className="text-left ">Message Status</th>
                  <th className="text-left">Total</th>
                  <th className="text-left">Rate</th>
                </tr>
                <tr>
                  <td>Sent</td>
                  <td>{overview.sent}</td>
                  <td>
                    {overview.messages == 0
                      ? 0
                      : (overview.sent * 100) / overview.messages}
                    %
                  </td>
                </tr>
                <tr>
                  <td>Delivered</td>
                  <td>{overview.pending}</td>
                  <td>
                    {overview.messages == 0
                      ? 0
                      : (overview.pending * 100) / overview.messages}
                    %
                  </td>
                </tr>
                <tr>
                  <td>Failed</td>
                  <td>{overview.failed}</td>
                  <td>
                    {overview.messages == 0
                      ? 0
                      : (overview.failed * 100) / overview.messages}
                    %
                  </td>
                </tr>
              </table>
            </div>
            <div className="bg-white h-auto lg:w-11/12 md:w-11/12 sm:w-11/12   pr-8 pb-20 rounded-lg ">
              <div className="flex mt-4 pt-4 w-3/4 border-b-2 border-solid pb-2 font-karla ml-6 ">
                <h1 className="pt-2">Recent Activity</h1>
              </div>
              <div>
                <div className="flex mt-2 ml-6 ">
                  <button className="bg-blue-400 rounded-lg text-white h-8 w-24 font-karla">
                    Sent
                  </button>
                  <button className="bg-white border-2 border-blue-400 rounded-lg ml-4 w-24 h-8 font-karla">
                    Recieved
                  </button>
                </div>
              </div>

              <div className="mt-4 ">
                <ResponsiveContainer width="100%" height={380}>
                  <LineChart
                    className="text-[10px]"
                    data={data}
                    margin={{ top: 25, right: 0, bottom: 0, left: 0 }}
                  >
                    <Tooltip />
                    <XAxis dataKey="label" />
                    <YAxis />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <Legend />
                    <Line type="monotone" dataKey="failed" stroke="red" />
                    <Line type="monotone" dataKey="recieved" stroke="#17A8F5" />
                    <Line type="monotone" dataKey="sent" stroke="#009355" />
                  </LineChart>
                </ResponsiveContainer>
              </div>
            </div>
          </div>
          {/* </div> */}
          <div className="block w-1/3 lg:w-full md:w-full sm:w-full ml-8 lg:mt-10 md:mt-10 font-inter text-sm">
            <div className="bg-white pt-8 pb-8 w-11/12 lg:w-11/12 md:w-11/12 sm:w-11/12 rounded-lg pl-8">
              <h1 className="font-bold">Recent Agents</h1>
              <p>{overview.agents} Agents</p>
              <div className="flex mt-6 gap-2">
                {overview.agentsList.map((item: any) => {
                  return (
                    <div className="h-12 w-12 rounded-full bg-slate-200 hover:border-b-4 hover:border-solid hover:border-blue-400 flex items-center justify-center">
                      {item.name[0]}
                    </div>
                  );
                })}
                <AiOutlineRight className="mt-4 ml-4" />
              </div>
            </div>
            <div className="bg-white  w-11/12 lg:w-11/12 md:w-11/12 sm:w-11/12  rounded-lg mt-8 pl-8 pb-8 mb-40">
              <h1 className="font-bold pt-6">Your groups</h1>
              <div className="h-fit  bg-white shadow-lg shadow-grey-200 mt-6 rounded-lg mr-2">
                {overview.groupsList.map((item: any) => {
                  return (
                    <div className="flex border-b-2 border-solid border-[#6C63FF] border-opacity-10 pb-1 pt-2 ml-2">
                      <div className="h-8 w-8 rounded-full bg-slate-200 hover:border-b-4 hover:border-solid hover:border-blue-400 flex items-center justify-center">
                        {item.name[0]}
                      </div>
                      <p className="pl-2 pt-1">{item.name}</p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      )}
      {type == "report" && (
        <div className="h-auto w-full mt-20">
          <div>
            <div className="flex justify-between ml-10">
              <h1 className="font-bold text-xl">Report</h1>
            </div>
            {displayingReport && (
              <div className="ml-10">
                <div className="mt-4">
                  {state.sent && (
                    <button className="bg-[#6C63FF] text-white hover:text-[#6C63FF] hover:bg-white hover:shadow-xl rounded-lg h-14 w-40 text-center font-karla text-lg">
                      Sent
                    </button>
                  )}
                  {state.recieved && (
                    <button className="bg-[#6C63FF] text-white hover:text-[#6C63FF] hover:bg-white hover:shadow-xl rounded-lg h-14 w-40 text-center ml-4 font-karla text-lg">
                      Recieved
                    </button>
                  )}
                </div>
                <div className="mt-12">
                  <form action="" onSubmit={getMessages}>
                    <input
                      type="date"
                      value={startDate?.toISOString().substring(0, 10) || ""}
                      onChange={handleStartDateChange}
                      placeholder="Start date"
                      className="h-14 w-56 border-solid border border-[#6C63FF] border-opacity-10 rounded-lg pl-4 pr-2 font-inter"
                    />
                    <input
                      type="date"
                      value={endDate?.toISOString().substring(0, 10) || ""}
                      onChange={handleEndDateChange}
                      placeholder="End date"
                      className="h-14 w-56 ml-4 border-solid border border-[#6C63FF] border-opacity-10 rounded-lg pl-4 pr-2 font-inter"
                    />
                  </form>
                </div>
                <div className="flex mt-6">
                  <select className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 font-inter  rounded-lg w-56 pl-3">
                    {messageType.map((option) => (
                      <option value={option.value}>{option.label}</option>
                    ))}
                    ;
                  </select>
                  {/* <select className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 font-inter rounded-lg w-56 pl-3  ml-4">
                    {messageStatus.map((option) => (
                      <option value={option.value}>{option.label}</option>
                    ))}
                    ;
                  </select> */}
                </div>
                <button
                  onClick={() => {
                    setDisplaySms(true), setDisplayingReport(false);
                  }}
                  className="h-14 bg-[#6C63FF] rounded-lg text-white w-48 mt-14 font-karla text-lg"
                >
                  Generate report
                </button>
              </div>
            )}
          </div>
          {sms && (
            <div className="h-full w-full" id="table">
              <button
                onClick={() => {
                  var element: any = document.getElementById("table");
                  element
                    .requestFullscreen()
                    .then(function () {
                      // element has entered fullscreen mode successfully
                    })
                    .catch(function (error: any) {
                      // element could not enter fullscreen mode
                      // error message
                      console.log(error.message);
                    });
                }}
              ></button>
              <div className="flex ml-10">
                <p>{startDate?.toISOString().substring(0, 10) || ""} -- </p>
                <p>{endDate?.toISOString().substring(0, 10) || ""}</p>
              </div>
              <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[80vw] lg:w-[70vw] mt-8 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
                <form className="flex items-center 3xl:w-1/4 2xl:w-1/4 xl:2/3 lg:ml-6 md:ml-6 sm:ml-6 lg:mr-4 md:mr-4 sm:mr-4">
                  <label className="sr-only">Search</label>
                  <div className="relative w-full">
                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                      <Search className="text-[#6C63FF]" />
                    </div>
                    <input
                      type="text"
                      id="simple-search"
                      className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                      placeholder="Search by name"
                      required
                      onChange={(e) => setSearchedVal(e.target.value)}
                    />
                  </div>
                  <button
                    type="submit"
                    className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    <Search />
                    <span className="sr-only">Search</span>
                  </button>
                </form>
                <div className="p-1.5 w-full inline-block align-middle">
                  <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100 overflow-x-auto overflow-hidden">
                    <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                      <thead className="bg-gray-50 font-karla">
                        <tr>
                          <th scope="col" className="px-6 py-3 text-left ">
                            Time
                          </th>
                          <th scope="col" className="px-6 py-3 text-left ">
                            Receiver
                          </th>
                          <th scope="col" className="px-6 py-3 text-left ">
                            Message
                          </th>

                          <th scope="col" className=" py-3 px-6 text-left">
                            Status
                          </th>
                          <th scope="col" className=" py-3 px-6 text-left">
                            options
                          </th>
                        </tr>
                      </thead>
                      <tbody className="divide-y divide-gray-200 font-inter">
                        {current
                          .filter((row) => {
                            if (!searchedVal.length) {
                              return true; // return all rows if search value is not provided
                            }
                            const text = row.text
                              ? row.text.toString().toLowerCase()
                              : ""; // convert name to string and lowercase it
                            const searched = searchedVal
                              .toString()
                              .toLowerCase(); // convert search value to string and lowercase it
                            return text.includes(searched); // return true if name includes the search value
                          })
                          .map((report_data) => (
                            <tr>
                              <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                {report_data.created_at}
                              </td>
                              <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap flex">
                                {report_data.receiver}
                              </td>
                              <td className="px-6 py-4 text-sm font-medium w-4 text-gray-800 ">
                                {report_data.text}
                              </td>

                              <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                {report_data.status === "DELIVERED" ? (
                                  <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-green-500">
                                    {report_data.status}
                                  </button>
                                ) : report_data.status === "PENDING" ? (
                                  <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-blue-500">
                                    {report_data.status}
                                  </button>
                                ) : (
                                  <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-red-500">
                                    {report_data.status}
                                  </button>
                                )}
                              </td>
                              <td className="px-6 py-4 text-sm whitespace-nowrap flex gap-2">
                                <button
                                  className="flex bg-white text-[#6C63FF] items-center text-center gap-2 font-karla"
                                  onClick={() => {
                                    resend(report_data);
                                  }}
                                >
                                  Resend
                                  <Send />
                                </button>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                    <div className="ml-auto mr-auto">
                      {isLoading ? <SpinnerLoader /> : null}
                    </div>
                  </div>
                  <Paginator
                    numPages={6}
                    data={message}
                    setCurrent={setCurrent}
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      )}

      {type == "bulkReport" && (
        <div className="h-auto w-full mt-20">
          <div>
            <div className="flex justify-between ml-10">
              <h1 className="font-bold text-xl"> Bulk SMS Report</h1>
              {/* <button onClick={()=>download(fileUrl,filename)} className="flex bg-white text-[#6C63FF] items-center text-center absolute right-6 font-karla gap-3 hover:text-blue-400">
              Download Report <Download />
            </button> */}
            </div>
            {displayingReport && (
              <div className="ml-10">
                <div className="mt-4">
                  <button className="bg-[#6C63FF] text-white hover:text-[#6C63FF] hover:bg-white hover:shadow-xl rounded-lg h-14 w-40 text-center font-karla text-lg">
                    Sent
                  </button>
                  <button className="bg-[#6C63FF] text-white hover:text-[#6C63FF] hover:bg-white hover:shadow-xl rounded-lg h-14 w-40 text-center ml-4 font-karla text-lg">
                    Recieved
                  </button>
                </div>
                <div className="mt-8">
                  <input
                    type="date"
                    placeholder="Start date"
                    className="h-14 w-56 border-solid border border-[#6C63FF] border-opacity-10 rounded-lg pl-4 pr-2 font-inter"
                  />
                  <input
                    type="date"
                    placeholder="End date"
                    className="h-14 w-56 ml-4 border-solid border border-[#6C63FF] border-opacity-10 rounded-lg pl-4 pr-2 font-inter"
                  />
                </div>
                <div className="flex mt-6">
                  <select className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 font-inter  rounded-lg w-56 pl-3">
                    {messageType.map((option) => (
                      <option value={option.value}>{option.label}</option>
                    ))}
                    ;
                  </select>
                  {/* <select className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 font-inter rounded-lg w-56 pl-3  ml-4">
                    {messageStatus.map((option) => (
                      <option value={option.value}>{option.label}</option>
                    ))}
                    ;
                  </select> */}
                </div>
                <button
                  onClick={() => {
                    setDisplaySms(true), setDisplayingReport(false);
                  }}
                  className="h-14 bg-[#6C63FF] rounded-lg text-white w-48 mt-14 font-karla text-lg"
                >
                  Generate report
                </button>
              </div>
            )}
          </div>
          {sms && (
            <div className="mt-10 h-full w-full">
              <div className="flex ml-10">
                <p>{startDate?.toISOString().substring(0, 10) || ""} -- </p>
                <p>{endDate?.toISOString().substring(0, 10) || ""}</p>
              </div>
              <div className="h-96 3xl:w-[87vw] 2xl:w-[80vw] xl:w-[80vw] lg:w-[70vw] mt-10 3xl:absolute 2xl:absolute xl:absolute lg:absolute right-4">
                <div className="p-1.5 w-full inline-block align-middle">
                  <div className="scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-100 overflow-x-auto overflow-hidden">
                    <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                      <thead className="bg-gray-50 font-karla">
                        <tr>
                          <th scope="col" className="px-3 py-3 text-left ">
                            Time
                          </th>
                          <th scope="col" className="px-6 py-3 text-left ">
                            Receiver
                          </th>
                          <th scope="col" className="px-12 py-3 text-left ">
                            Message
                          </th>

                          <th scope="col" className=" py-3 px-6 text-left">
                            Status
                          </th>
                          <th scope="col" className=" py-3 px-6 text-left">
                            options
                          </th>
                        </tr>
                      </thead>
                      <tbody className="divide-y divide-gray-200 font-inter">
                        {current?.map((report_data: any) => (
                          <tr>
                            <td className="px-3 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {report_data.created_at}
                            </td>
                            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap flex">
                              {report_data.recipient.length}
                            </td>
                            <td className="px-3 py-4 text-sm font-medium w-4 text-gray-800 ">
                              {report_data.text}
                            </td>

                            <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                              {report_data.status === "DELIVERED" ? (
                                <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-green-500">
                                  {report_data.status}
                                </button>
                              ) : report_data.status === "PENDING" ? (
                                <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-blue-500">
                                  {report_data.status}
                                </button>
                              ) : (
                                <button className="bg-[#4CAF50] bg-opacity-5 h-8 w-32 rounded-lg text-red-500">
                                  {report_data.status}
                                </button>
                              )}
                            </td>
                            <td className="px-6 py-4 text-sm whitespace-nowrap flex gap-2">
                              <CloudDownload
                                className="text-xl text-green-400"
                                onClick={() => {
                                  let row: string[] = [];
                                  for (
                                    let i = 0;
                                    i < report_data.recipient.length;
                                    i++
                                  ) {
                                    row.push(report_data.recipient.tel);
                                  }
                                  exportTableToCSV(row, `receivers.csv`);
                                }}
                              />
                              <button
                                className="flex bg-white text-[#6C63FF] items-center text-center gap-2 font-karla"
                                onClick={() => {
                                  resend(report_data);
                                }}
                              >
                                Resend
                                <Send />
                              </button>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                    <div className="ml-auto mr-auto">
                      {isLoading ? <SpinnerLoader /> : null}
                    </div>
                  </div>

                  <Paginator
                    numPages={6}
                    data={bulkReport}
                    setCurrent={setCurrent}
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default Dashboard;
