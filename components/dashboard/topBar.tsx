import Image from "next/image";
import { useState, useReducer, useRef, useEffect } from "react";
import { FaBars } from "react-icons/fa";
import { X, Search, XCircle } from "react-bootstrap-icons";
import router from "next/router"
import io from "socket.io-client";

import NewSideBar from "./newSideBar";
import {
  Bell,
  Gear,
} from "react-bootstrap-icons";
import { api } from "../utils/api";
import useAuth from "../context/authContext";
import useResponse from "../context/responseContext";
import useOutsideRef from "../utils/wrapperRef";
import { FiLogOut } from "react-icons/fi";

type ACTIONTYPE = { type: "notification" } | { type: "request" };

const initialEvent = {
  notification: true,
  request: false,
};
const handleEvents = (state: typeof initialEvent, action: ACTIONTYPE) => {
  state = {
    notification: false,
    request: false,
  };
  switch (action.type) {
    case "notification":
      return {
        ...state,
        notification: true,
      };
    case "request":
      return {
        ...state,
        request: true,
      };
    default:
      return {
        ...state,
      };
  }
};
export default function TopNavbar() {
  const [notifications, setNotification] = useState<Boolean>(false);
  const [notify, dispatch] = useReducer(handleEvents, initialEvent);
  const [list, setList] = useState<Array<any>>([]);

  const [newSidebar, setNewSidebar] = useState<Boolean>(false);
  const [requests, setRequests] = useState<[]>([]);
  const socket = io(process.env.NEXT_PUBLIC_BEEP_IN_SOCKET_URL!);
  const [newNotification, setNew ] = useState(true);
  const getTopupReport = () => {
    api
      .get(`/topup/get/REQUESTED`)
      .then((res) => {
        setRequests(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const wrapperRef = useRef(null);
  useOutsideRef(wrapperRef, setNotification);
  const { user } = useAuth();
  const { updateSuccess, updateMessage } = useResponse();
  const processTopup= async (
    id: number,
    agent_id: string,
    process: string
  ) => {
    await api
      .post(`topup/process/${id}/${process}`)
      .then((res) => {
        socket.emit("processTopup", {
          sender: user?.id,
          receiver: agent_id,
          message: `Your Topup has been ${process}D`,
          category: "TOPUP",
        });
        updateSuccess();
        updateMessage(res.data.message);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    socket.on("new_notification", () => {
      setNew(true);
    });
  }, []);
  // const { user, logout } = useAuth();
  
 
  return (
    <div>
      <div className=" w-full h-[6vh] mt-4 border-b-2 pb-4">
        <div className="flex z-50 3xl:gap-0 2xl:gap-0 xl:gap-0 lg:gap-0 md:gap-36 sm:gap-36 ml-8 items-center">
          {newSidebar ? (
            <button onClick={() => setNewSidebar(false)}>
              <X className="text-xl 3xl:hidden 2xl:hidden xl:hidden lg:hidden" />
            </button>
          ) : (
            <button onClick={() => setNewSidebar(true)}>
              <FaBars className="text-xl 3xl:hidden 2xl:hidden xl:hidden lg:hidden" />
            </button>
          )}
          <div className="flex gap-24 w-full">
            <img src="/icons/logo.svg" alt="logo" className="3xl:ml-2" />

            <div className="3xl:flex 2xl:flex xl:flex w-full lg:flex md:hidden sm:hidden">
            <form className="flex items-center">
                <label className="sr-only">Search</label>
                <div className="relative w-full">
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <Search className="text-[#6C63FF]" />
                  </div>
                  <input
                    type="text"
                    id="simple-search"
                    className=" border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5"
                    placeholder="Search"
                    required
                  />
                </div>
                <button
                  type="submit"
                  className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  <Search />
                  <span className="sr-only">Search</span>
                </button>
              </form>
            </div>

            <div className="flex 3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden right-10  gap-4">
              <div>
                <div
                  className={`h-2 w-2 absolute top-2 ml-4 rounded-full  ${
                    newNotification ? "bg-red-500" : "bg-green-500"
                  }`}
                ></div>
                <Bell
                  onClick={async () => {
                    await api.get("/notifications/get").then((response) => {
                      setList(response.data.notifications);
                      setNew(false);
                    });
                    getTopupReport();
                    setNotification(true);
                  }}
                  className="mt-2 text-2xl"
                />
              </div>
              <div className="flex">
                <Image
                  height={300}
                  width={40}
                  src={"/images/profile.jpg"}
                  alt="profile"
                  className="rounded-full h-10"
                />
                {/* <ChevronDown className="mt-4 ml-1" /> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      {notifications && (
        <div
          className="h-auto absolute z-50 w-2/5 bg-white rounded-lg  right-10 pb-10 border-2 border-solid border-gray-200 pt-6"
          ref={wrapperRef}
        >
          <div className="flex justify-between border-b-2 border-solid border-[#D9D9D9] pb-4">
            <button onClick={() => dispatch({ type: "notification" })}>
              <h1 className="pl-8">NOTIFICATIONS({list.length})</h1>
            </button>
            <button onClick={() => dispatch({ type: "request" })}>
              <h1 className="pr-8">REQUESTS({requests.length})</h1>
            </button>
          </div>
          {notify.notification && (
            <div className="h-auto opacity-70 flex flex-col">
              {list.map((item) => {
                return <h1 className="bg-[#D9D9D9] text-black-900  pl-8  ml-8 mr-8 rounded-lg mt-6 pt-4 pb-4">{item.message}</h1>;
              })}
            </div>
          )}
          {notify.request && (
            <div className=" h-64 overflow-y-scroll scrollbar-hide">
              {requests.map((item: any) => {
                return (
                  <div className="flex items-center justify-between pt-8">
                    <div className="flex items-center ml-8">
                      <div className="h-14 w-14 rounded-full bg-[#D9D9D9] flex justify-center items-center border drop-shadow border-[#6C63FF]">
                        {item.name[0]}
                      </div>
                      <p className="font-karla pl-4">
                        Top up request from {item.name}
                      </p>
                    </div>
                    <div className="flex mr-8">
                      <button
                        className="h-10 w-24 mr-4 rounded-sm bg-[#6C63FF] text-white font-karla"
                        onClick={() => {
                          processTopup(item.id, item.agent_id, "APPROVE");
                        }}
                      >
                        Approve
                      </button>
                      <button
                        className="h-10 w-24 rounded-sm bg-white text-[#6C63FF] font-karla shadow-md shadow-[#6C63FF]"
                        onClick={() => {
                          processTopup(item.id, item.issued_to, "DECLINE");
                        }}
                      >
                        Decline
                      </button>
                    </div>
                  </div>
                );
              })}
            </div>
          )}
        </div>
      )}
      {newSidebar && (
        <NewSideBar />
      )

      }
     
    </div>
  );
}
