import { useState, useEffect, useReducer, useRef } from 'react';
import Link from 'next/link';
import useAuth from '../context/authContext';
import { api } from '../utils/api';
import {
  Gear,
  XCircle,
} from 'react-bootstrap-icons';
import {FiLogOut} from 'react-icons/fi'
import UpdateForms from '../forms/updateForms';
import router,{useRouter} from 'next/router';
import { admin, agent, reseller, superReseller } from './menu';
// type ACTIONTYPE =
//   | { type: 'messages' }
//   | { type: 'groups' }
//   | { type: 'topup' }
//   | { type: 'overview' }
//   | { type: 'topupHistory' }
//   | { type: 'topupReport' }
//   | { type: 'buySms' };
// const initialDisplay = {
//   messages: false,
//   groups: false,
//   topup: false,
//   overview: false,
// };
// const inititialHistory = {
//   topupReport: false,
//   topupHistory: false,
//   buySms: false,
// };
// const topupDisplay = (topup: typeof inititialHistory, action: ACTIONTYPE) => {
//   topup = {
//     topupHistory: false,
//     buySms: false,
//     topupReport: false,
//   };
//   switch (action.type) {
//     case 'topupHistory':
//       return {
//         ...topup,
//         topupHistory: true,
//       };
//     case 'topupReport':
//       return {
//         ...topup,
//         topupReport: true,
//       };
//     case 'buySms':
//       return {
//         ...topup,
//         buySms: true,
//       };
//     default:
//       return {
//         ...topup,
//       };
//   }
// };
// enum filter {
//   APPROVED = 'APPROVED',
//   DECLINED = 'DECLINED',
//   PENDING = 'REQUESTED',
//   ALL = 'ALL',
// }
// const dispalayEvents = (state: typeof initialDisplay, action: ACTIONTYPE) => {
//   state = {
//     messages: false,
//     groups: false,
//     topup: false,
//     overview: false,
//   };
//   switch (action.type) {
//     case 'messages':
//       return {
//         ...state,
//         messages: true,
//       };
//     case 'groups':
//       return {
//         ...state,
//         groups: true,
//       };
//     case 'topup':
//       return {
//         ...state,
//         topup: true,
//       };
//     case 'overview':
//       return {
//         ...state,
//         overview: true,
//       };
//     default:
//       return {
//         ...state,
//       };
//   }
// };
const LeftSidebar = () => {
  const { user, logout } = useAuth();
  // const [topup, display] = useReducer(topupDisplay, inititialHistory);
  const [loggingOut, setLoggingOut] = useState<Boolean>(false);
  const [settings, setSettings] = useState<Boolean>(false);
  const [deleteAccount, setDeleteAccount] = useState<Boolean>(false);
  // const [messageDisplay, setMessageDisplay] = useState<Boolean>(false);
  const [password, setPassword] = useState<any>(null);
  // const [state, dispatch] = useReducer(dispalayEvents, initialDisplay);
  const menu =  user?.position_id == 1?admin: user?.position_id == 2 ? superReseller : user?.position_id == 3 ? reseller : agent;

  const logOut = () => {
    logout();
  };
  const url = `/agent/delete/${user?.id}`;
  const deletingAccount = async () => {
    const data: any = { password };
    await api.post(url, data).then((res) => {
      logout();
      router.push('/auth/login');
    });
  };
useEffect(()=>{ 
    window.onclick = function(event : any) {
      if (!event.target.matches('.dropbtn')) {  
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (!openDropdown.classList.contains('hidden')) {
          openDropdown.classList.add('hidden');
        }
      }
    }
  }
})
const router = useRouter();
const {type} = router.query;
  return (
    <div>
      <div className="3xl:flex 2xl:flex xl:flex h-screen lg:flex md:hidden sm:hidden">
        <div className="w-52  bg-blue-100 bg-opacity-40  pt-28 h-screen  flex gap-9 flex-col">
          {/* <div className="flex">
            <Link href="messageHandler">
              <div
                onClick={() => dispatch({ type: 'messages' })}
                className="flex ml-8 mt-10 hover:text-[#6C63FF] hover:border-r-4 hover:border-solid hover:border-[#6C63FF]"
              >
                <FaRegComments className=" rounded-l-lg h-6  font-light focus:text-[#6C63FF] focus:border-r-8 focus:border-solid focus:border-[#6C63FF]" />
                <p className="font-karla pl-2">Messages</p>
              </div>
            </Link>
            {state.messages && (
              <div className="absolute bg-white 3xl:left-48 2xl:left-24 xl:left-24 lg:left-24 md:right-24 sm:right-24 pt-6 top-[30%] list-none flex gap-8 flex-col  rounded-lg shadow-md shadow-indigo-500/40 h-auto pb-6">
                <Link
                  href={{
                    pathname: 'messageHandler',
                    query: { type: 'single' },
                  }}
                >
                  <button className="flex hover:text-[#6C63FF] w-auto focus:h-12 items-center pr-28 focus:bg-gradient-to-r from-blue-100 via-sky-100 focus:bg-opacity-5">
                    <AiOutlineRight className="mt-1 ml-2" />
                    <li className="pl-4 font-karla">Single</li>
                  </button>
                </Link>
                <Link
                  href={{ pathname: 'messageHandler', query: { type: 'bulk' } }}
                >
                  <button className="flex hover:text-[#6C63FF] w-auto focus:h-12 items-center pr-28 focus:bg-gradient-to-r from-blue-100 via-sky-100 focus:bg-opacity-5">
                    <AiOutlineRight className="mt-1 ml-2" />
                    <li className="pl-4 font-karla">Bulk</li>
                  </button>
                </Link>
                <Link
                  href={{
                    pathname: 'messageHandler',
                    query: { type: 'group' },
                  }}
                >
                  <button className="flex hover:text-[#6C63FF] w-auto focus:h-12 items-center pr-28 focus:bg-gradient-to-r from-blue-100 via-sky-100 focus:bg-opacity-5">
                    <AiOutlineRight className="mt-1 ml-2" />
                    <li className="pl-4 font-karla">Group</li>
                  </button>
                </Link>
              </div>
            )}
          </div> */}

          {menu.map((item) => {
            return (
              <div>
                <div
                  id="dropdownDelayButton"
                  
                  onClick={() => {
                    document
                      .getElementById(item.id)
                      ?.classList.remove('hidden');
                  }}
                  className="dropbtn flex ml-8 mt-2 hover:text-[#6C63FF] hover:border-r-4 hover:border-solid hover:border-[#6C63FF]"
                >
                
                {item.icon}
                  <p className="font-karla pl-2">{item.link}</p>
                </div>
                <div
                  id={item.id}
                  className="dropdown-content z-50 hidden absolute left-52 bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700"
                >
                  <ul
                    className="py-2 text-sm text-gray-700 dark:text-gray-200"
                    aria-labelledby="dropdownDelayButton"
                  >
                    {
                      item.nestedLinks.map((item)=>{return( 
                        <Link href={item.href}>
                        <p
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                        >
                          {item.link}
                        </p>
                      </Link>
                      )})
                    }
            
                  </ul>
                </div>
              </div>
            );
          })}

          <div className="w-full z-0 py-52 pl-8">
            {/* <Link href={{pathname:"",query:{type:"settings"}}}> */}
            <button
              onClick={() => {setSettings(true),setTimeout(() =>{},1000)}}
              className="flex w-full  hover:text-[#6C63FF] hover:border-r-4 hover:border-solid   hover:border-[#6C63FF]"
            >
              <Gear className="h-6  rounded-l-lg text-sm font-light" />
              <p className="font-karla pl-2">Settings</p>
            </button>
            {/* </Link> */}
            {/* <Link href={{pathname:"", query:{type:"logOut"}}}> */}
            <button
              onClick={() => setLoggingOut(true)}
              className="flex w-full  mt-6  hover:text-[#6C63FF] hover:border-r-4 hover:border-solid   hover:border-[#6C63FF]"
            >
              <FiLogOut className=" rounded-l-lg text-sm font-light h-6" />
              <p className="font-karla pl-2">Logout</p>
            </button>
            {/* </Link> */}
          </div>
          {loggingOut &&(
            <div className="w-full absolute top-0 bottom-0 z-50 bg-gray-800 opacity-90">
              <div className="h-40 w-96 bg-white mr-auto ml-20 mt-[36%] pt-10">
                <h4 className="text-lg text-center font-karla font-semibold">
                  Are you sure you want to logout?
                </h4>
                <div className="flex justify-evenly mt-8">
                  <button
                    onClick={()=> setLoggingOut(false)}
                    className="h-10 w-20 rounded-lg border-2 border-solid border-blue-500"
                  >
                    No
                  </button>
                  <button
                    onClick={() => logOut()}
                    className="h-10 w-20 rounded-lg border-2 border-solid border-blue-500"
                  >
                    Yes
                  </button>
                </div>
              </div>
            </div>
          )}
          { settings  && (
            <div className="h-32 w-48 rounded-xl bg-white  absolute left-52  bottom-32 shadow-md shadow-gray-200">
              <div className="block">

                  <Link href={{pathname:"updateUser", query:{type:"editProfile"}}}>
                <button className="h-10 w-full mt-6 text-sm font-inter hover:bg-gray-100">
                  Edit profile
                </button>
                  </Link>
                  {/* <Link href={{pathname:"",query:{type:"deleteAccount"}}}> */}
                <button 
                onClick={()=>setDeleteAccount(true)}
                  className="h-10 w-full text-sm font-inter hover:bg-gray-100"
                >
                  Delete Account
                </button>
                {/* </Link> */}
              </div>
            </div>
          )}
        </div>
        {deleteAccount && (
          <div className="h-screen w-full ml-52 bg-gray-800 absolute right-0 top-0 opacity-90 z-50">
            <div className="h-auto pb-[4%] w-[40vw] bg-white ml-auto mr-auto mt-[10%] backdrop:blur-3xl">
              <XCircle
                className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
                onClick={() => setDeleteAccount(false)}
              />
              <h1 className="text-2xl font-bold font-karla text-center pt-6">
                Delete my Account
              </h1>
              <p className=" text-center font-karla pl-20 pr-20 pt-6 pb-6">
                Please enter the password to confirm your account deletion
              </p>
              <form action="">
                <input
                  onChange={(e: any) => setPassword(e.target.value)}
                  type="password"
                  placeholder="Password"
                  className=" block border-solid border border-[#6C63FF] border-opacity-50 h-14  w-3/4 rounded-lg pl-8 font-inter ml-20 mt-3"
                />
                <div className="flex justify-center">
                  <button
                    onClick={() => deletingAccount()}
                    type="button"
                    className="h-14 w-40 text-white bg-[#6C63FF] rounded-lg font-karla mt-4"
                  >
                    Delete Account
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default LeftSidebar;
