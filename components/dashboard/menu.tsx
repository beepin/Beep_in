import { Folder, JournalCheck, People,CardChecklist } from "react-bootstrap-icons"
import { AiOutlineBarChart } from "react-icons/ai"
import { FaRegComments } from "react-icons/fa"

export const admin = [
    {
        id: "overview",
        link: 'Overview',
        icon: <AiOutlineBarChart className="  h-6  rounded-l-lg  font-light" />,
        nestedLinks: [
            {
                link: 'Overview',
               href: { pathname: "dashboardView", query: { type: "overview" } }

            },
        
            {
                link: "Message Report",
               href: { pathname: "dashboardView", query: { type: "report" } }

            }
        ],
        

    },
    {
        id: "messages",
        link : 'Messages',
        icon: <FaRegComments className=" rounded-l-lg h-6  font-light focus:text-[#6C63FF] focus:border-r-8 focus:border-solid focus:border-[#6C63FF]" />,
        nestedLinks: [
            {
               link: "Bulk Message Report",
               href: { pathname: "dashboardView", query: { type: "bulkReport" } }
            }
        ]
    },
    {
        id: "agents",
        icon:  <JournalCheck className="   h-6  rounded-l-lg  font-light" />,
        link : 'Agent list',
        nestedLinks: [
            {
                
               link: "Agent List",
               href: { pathname: "detailView", query: { type: "agentList" } }

            },
          
        ]
    },
    {
        id: "groups",
        link: 'Groups',
        icon :  <People className="h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Create Group",
               href: { pathname: "groupManagement", query: { type: "newGroup" } }

            },
            {
                link: "Groups List",
               href: { pathname: "groupManagement", query: { type: "groupsList" } }

            },
        ]
    },
    {
        id: "topup",
        link: 'Topup details',
        icon: <Folder className="  h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            // {
            //    link: "Issue top up",
            //    href: { pathname: "messageHandler", query: { type: "single" } }

            // },
            {
                link: "Top up report",
               href: { pathname: "topUpDetails", query: { type: "topupReport" } }

            }
        ]
    },

    {
        id:"apis",
        link:"View apis",
        icon: <CardChecklist className="h-6  rounded-l-lg  font-light" />,
        nestedLinks:[
            {

                link:"View Apis",
                href: {pathname: "apiViewer", query: { type: "viewApis"}}
            }

        ]
    }

]

export const superReseller = [
    {
        id: "overview",
        link: 'Overview',
        icon: <AiOutlineBarChart className="  h-6  rounded-l-lg  font-light" />,
        nestedLinks: [
            {
                link: 'Overview',
               href: { pathname: "dashboardView", query: { type: "overview" } }

            },
            {
               link: "Bulk Message Report",
               href: { pathname: "dashboardView", query: { type: "bulkReport" } }

            },
            {
                link: "Message Report",
               href: { pathname: "dashboardView", query: { type: "report" } }

            }
        ]
    },
    {
        id: "messages",
        link : 'Messages',
        icon: <FaRegComments className=" rounded-l-lg h-6  font-light focus:text-[#6C63FF] focus:border-r-8 focus:border-solid focus:border-[#6C63FF]" />,
        nestedLinks: [
            {
               link: "Single",
               href: { pathname: "messageHandler", query: { type: "single" } }
            },
            {
                link: "Bulk",
                href: { pathname: "messageHandler", query: { type: "bulk" } }
            },
            {
                link: "Group",
                href: { pathname: "messageHandler", query: { type: "group" } }
            },
        ]
    },
    {
        id: "agents",
        icon:  <JournalCheck className="   h-6  rounded-l-lg  font-light" />,
        link : 'Agents',
        nestedLinks: [
            {
               link: "Agent List",
               href: { pathname: "detailView", query: { type: "agentList" } }

            },
            {
                link: "Add Agent",
               href: { pathname: "../auth/agentCreate" }

            },
        ]
    },
    {
        id: "groups",
        link: 'Groups',
        icon :  <People className="h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Create Group",
               href: { pathname: "groupManagement", query: { type: "newGroup" } }

            },
            {
                link: "Groups List",
               href: { pathname: "groupManagement", query: { type: "groupsList" } }

            },
        ]
    },
    {
        id: "topup",
        link: 'Topup details',
        icon: <Folder className="  h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Issue top up",
               href: { pathname: "topUpDetails", query: { type: "issuingTopup" } }

            },
            {
                link: "Top up report",
               href: { pathname: "topUpDetails", query: { type: "topupReport" } }

            }
        ]
    },


]

export const reseller = [
    {
        id: "overview",
        link: 'Overview',
        icon: <AiOutlineBarChart className="  h-6  rounded-l-lg  font-light" />,
        nestedLinks: [
            {
                link: 'Overview',
               href: { pathname: "dashboardView", query: { type: "overview" } }

            },
            {
               link: "Bulk Message Report",
               href: { pathname: "dashboardView", query: { type: "bulkReport" } }

            },
            {
                link: "Message Report",
               href: { pathname: "dashboardView", query: { type: "report" } }

            }
        ]
    },
    {
        id: "messages",
        link : 'Messages',
        icon: <FaRegComments className=" rounded-l-lg h-6  font-light focus:text-[#6C63FF] focus:border-r-8 focus:border-solid focus:border-[#6C63FF]" />,
        nestedLinks: [
            {
               link: "Single",
               href: { pathname: "messageHandler", query: { type: "single" } }
            },
            {
                link: "Bulk",
                href: { pathname: "messageHandler", query: { type: "bulk" } }
            },
            {
                link: "Group",
                href: { pathname: "messageHandler", query: { type: "group" } }
            },
        ]
    },
    {
        id: "agents",
        icon:  <JournalCheck className="   h-6  rounded-l-lg  font-light" />,
        link : 'Agent list',
        nestedLinks: [
            {
               link: "Agent List",
               href: { pathname: "../dashboard/detailView", query: { type: "agentList" } }

            },  
            {
                link: "Add Agent",
               href: { pathname: "../auth/agentCreate" }

            },
        ]
    },
    {
        id: "groups",
        link: 'Groups',
        icon :  <People className="h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Create Group",
               href: { pathname: "groupManagement", query: { type: "newGroup" } }

            },
            {
                link: "Groups List",
               href: { pathname: "groupManagement", query: { type: "groupsList" } }

            },
        ]
    },
    {
        id: "topup",
        link: 'Topup details',
        icon: <Folder className="  h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Issue top up",
               href: { pathname: "topUpDetails", query: { type: "issuingTopup" } }

            },
            {
                link: "Top up report",
               href: { pathname: "topUpDetails", query: { type: "topupReport" } }

            }
        ]
    },
   

]

export const agent = [
    {
        id: "overview",
        link: 'Overview',
        icon: <AiOutlineBarChart className="  h-6  rounded-l-lg  font-light" />,
        nestedLinks: [
            {
                link: 'Overview',
               href: { pathname: "dashboardView", query: { type: "overview" } }

            },
            {
               link: "Bulk Message Report",
               href: { pathname: "dashboardView", query: { type: "bulkReport" } }

            },
            {
                link: "Message Report",
               href: { pathname: "dashboardView", query: { type: "report" } }

            }
        ]
    },
    {
        id: "messages",
        link : 'Messages',
        icon: <FaRegComments className=" rounded-l-lg h-6  font-light focus:text-[#6C63FF] focus:border-r-8 focus:border-solid focus:border-[#6C63FF]" />,
        nestedLinks: [
            {
               link: "Single",
               href: { pathname: "messageHandler", query: { type: "single" } }
            },
            {
                link: "Bulk",
                href: { pathname: "messageHandler", query: { type: "bulk" } }
            },
            {
                link: "Group",
                href: { pathname: "messageHandler", query: { type: "group" } }
            },
        ]
    },
    {
        id: "groups",
        link: 'Groups',
        icon :  <People className="h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
               link: "Create Group",
               href: { pathname: "groupManagement", query: { type: "newGroup" } }

            },
            {
                link: "Groups List",
               href: { pathname: "groupManagement", query: { type: "groupsList" } }

            },
        ]
    },
    {
        id: "topup",
        link: 'Topup details',
        icon: <Folder className="  h-6  rounded-l-lg  font-light " />,
        nestedLinks: [
            {
                link: "Top up report",
                href: { pathname: "topUpDetails", query: { type: "topupReport" } }

            },
            {
                link: "Request top up",
                href: { pathname: "topUpDetails", query: { type: "issuingTopup" } }
 
             },
        ]
    },


]
