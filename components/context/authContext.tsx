import { getCookie, setCookie, removeCookies } from 'cookies-next';
import { useRouter } from 'next/router';
import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';
import { api } from '../utils/api';
import jwt_decode from 'jwt-decode';
import useResponse from './responseContext';
import useLoader from './loaderContext';
import { disable } from '../utils/disableButton';
import { log } from 'console';
import useStateCallback from '../utils/useStateCallback';

export interface user {
  id: string;
  name: string;
  phone_number: string;
  created_by: string;
  email: string;
  address: string;
  position_id: number;
  total_sms: number;
  status: string;
  created_at: string;
  groups: Array<any>;
  buying_price: number,
  TIN: string;
}
export type authContextType = {
  user: user | null;
  login: (data: LoginInput, button: any) => void;
  logout: () => void;
  updateUser: () => void;
  updateProfile: (userData:user) => void;
  priv: { privilege: string; position: number }[] | null;
};
const authContextDefaultValues: authContextType = {
  user: null,
  login: (data: LoginInput) => { },
  logout: () => { },
  updateUser: () => { },
  updateProfile: () => { },
  priv: null,
};
export const AuthContext = createContext<authContextType>(
  authContextDefaultValues,
);
interface LoginInput {
  email: string;
  password: string;
}
interface Props {
  children: ReactNode;
}
export const Priv = [
  { privilege: 'ADMIN', position: 1 },
  { privilege: 'SUPER RESELLER', position: 2 },
  { privilege: 'RESELLER', position: 3 },
  { privilege: 'AGENT', position: 4 },
];
export function AuthProvider({ children }: Props) {
  const [user, setUser] = useStateCallback<(user) | null>(null);
  const [priv, setPriv] = useState<{ privilege: string; position: number }[] | null >(null);
  // const [updates,setUpdates] = useState<userDataUpdate>();
  const { updateMessage, updateSuccess, updateFailure } = useResponse();
  const { updateLoading } = useLoader()
  const router = useRouter();

  const updateUser = () => {
    const token: string = getCookie('accessToken') as string;
    const decoded: any = jwt_decode(token);
    api.get(`/agent/id/${decoded.id}`).then((res) => {
      setUser(res.data.data);
      setPriv(Priv.filter((priv) => priv.position > res.data.data.position_id));
    });
  };
  const updateProfile = (userData: user) => {
    const token: string = getCookie('accessToken') as string;
    const decoded: any = jwt_decode(token);
    const userId = decoded.id;
   
    const requestBody = {
        name: userData.name,
        phone_number: userData.phone_number,
        email: userData.email
      
    }
    console.log(requestBody);
    
    api.put(`/agent/update/id/${userId}`, requestBody).then((res) => {
      setUser(res.data.userData)
      console.log("Successfully updated user:", res);
    }).catch((err) => {
      console.log("Error updating user:", err);
    });
  }
  
  React.useEffect(() => {
    updateLoading(false)
    const token = getCookie('accessToken');
    if (!token) {
      if (router.pathname !== '/' && router.pathname !== '/auth/signup')
        router.push('/auth/login');
    } else {
      if (router.pathname == '/auth/login' || router.pathname == '/dashboard/messageHandler')
        router.push('/dashboard/messageHandler')
      updateUser();
    }
  }, [router.pathname]);
  const login = async (values: LoginInput, button: any) => {
    updateLoading(true);
    await api
      .post(`/agent/login`, values)
      .then(({ data }) => {
        setUser(data.user);

        setCookie('accessToken', data.access_token, {
          path: '/',
          maxAge: 3600, // Expires after 1hr
          sameSite: true,
        });
        setCookie('refreshToken', data.refresh_token, {
          path: '/',
          maxAge: 3600, // Expires after 1hr
          sameSite: true,
        });
        router.push('/dashboard/dashboardView?type=overview');
        updateSuccess();
        updateMessage(data.message);
        updateLoading(false);
        setPriv(Priv.filter((priv) => priv.position > data.user.position_id));
      })
      .catch((err) => {
        disable(button);
        updateLoading(false)
      });
  };

  const logout = () => {
    setUser({
      id: '',
      name: '',
      phone_number: '',
      email: '',
      created_by: '',
      position_id: 0,
      total_sms: 0,
      status: '',
      created_at: '',
      address: '',
      TIN: '',
      buying_price: 0,
      groups: [],
    });
    removeCookies('accessToken')
    removeCookies('refreshToken')
    router.push('/auth/login');
  };
  const value = { 
    updateUser,
    updateProfile,
    user,
    login,
    logout,
    priv,
    
  };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
export default function useAuth() {
  return useContext(AuthContext);
}
