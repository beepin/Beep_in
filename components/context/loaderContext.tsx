// import { type } from "os";

import { useRouter } from 'next/router';
import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';

export type LoaderContextType = {
  loading: boolean | null;
  updateLoading: (data:boolean) => void;
};
const authContextDefaultValues: LoaderContextType = {
  loading: null,
  updateLoading: (data: boolean) => {},
};
export const LoaderContext = createContext<LoaderContextType>(
  authContextDefaultValues,
);

interface LoginInput {
  email: string;
  password: string;
}

interface Props {
  children: ReactNode;
}

export function LoaderProvider({ children }: Props) {
  const [loading, setLoading] = useState<boolean>(false);

  const updateLoading = (value:boolean) => {
    setLoading(value);
  };

  const value = {
    loading,
    updateLoading
  };

  return (
    <LoaderContext.Provider value={value}>{children}</LoaderContext.Provider>
  );
}

export default function useLoader() {
  return useContext(LoaderContext);
}
