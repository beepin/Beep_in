import React, { FormEvent, useEffect, useState } from "react";
import { getCookie, setCookie } from "cookies-next";
import { useRouter } from "next/router";
import useAuth, { user } from "../context/authContext";
import { useForm } from "react-hook-form";
import useResponse from "../context/responseContext";

function UpdateForms() {
  const { user }: any = useAuth();
  const [editUser, setEditUser] = useState<user>();
  const [input, setInput] = useState<string>();
  const { register, handleSubmit } = useForm<user>();
  const { updateSuccess, updateMessage, updateFailure } = useResponse();
  const router = useRouter();
  const { type } = router.query;
  const { updateProfile } = useAuth();
  useEffect(() => {
    setEditUser(user);
  }, [user]);
  const handleFormSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    updateProfile(editUser as user);
  };
  return (
    <div className="w-full font-karla">
      <div className="h-20 w-full border-b-2 border-solid flex justify-center float-right items-center text-[#6C63FF] text-xl ">
        <h1>Update Account</h1>
      </div>

      {user?.position_id == 1 ||
      user?.position_id == 2 ||
      user?.position_id == 3 ||
      user?.position_id == 4
        ? type == "editProfile" && (
            <div>
              <div className="h-96 3xl:w-1/2 2xl:w-1/2 xl:w-1/2 lg:w-2/3 md:w-2/3 sm:w-3/4 ml-auto mr-auto mt-48">
                <form
                  action=""
                  className="w-full md:pl-8 ml-auto mr-auto"
                  onSubmit={handleFormSubmit}
                >
                  <div className="flex w-full mt-10">
                    <input
                      {...register("name")}
                      name="name"
                      type="text"
                      value={editUser?.name}
                      onChange={(e) =>
                        setEditUser({
                          ...editUser,
                          name: e.target.value,
                        } as user)
                      }
                      className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                    />
                  </div>
                  <div className="flex w-full mt-4">
                    <input
                      {...register("email")}
                      name="email"
                      type="email"
                      
                      value={editUser?.email}
                      onChange={(e) =>
                        setEditUser({
                          ...editUser,
                          email: e.target.value,
                        } as user)
                      }
                      className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-full rounded-lg pl-8 "
                    />
                  </div>
                  <div className="flex w-full mt-4">
                    <input
                      {...register("phone_number")}
                      name="phone_number"
                      type="tel"
                      value={editUser?.phone_number}
                      onChange={(e) =>
                        setEditUser({
                          ...editUser,
                          phone_number: e.target.value,
                        } as user)
                      }
                      className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8"
                    />
                  </div>
                 
                  <div className="flex w-full mt-4">
                    <input
                      name="position"
                      type="text"
                      value={user.position_id == 1 ? 'ADMIN':user.position_id == 2 ? 'SUPER RESELLER':user.position_id == 3 ? 'RESELLER':'AGENT'}
                       readOnly
                      className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-full rounded-lg pl-8"
                    />
                  </div>
                  <div className="mt-10 flex justify-between">
                    <button
                      type="button"
                      className="h-14 w-28 shadow-sm rounded-xl shadow-slate-500 bg-white text-[#6C63FF]"
                    >
                      Cancel
                    </button>
                    <button
                      type="submit"
                      className="h-14 w-28 bg-[#6C63FF] text-white rounded-xl "
                    >
                      Update
                    </button>
                  </div>
                </form>
              </div>
            </div>
          )
        : ""}
    </div>
  );
}

export default UpdateForms;
