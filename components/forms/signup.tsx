import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import useLoader from "../context/loaderContext";
import useResponse from "../context/responseContext";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import { api } from "../utils/api";
import { disable } from "../utils/disableButton";

interface FormInput {
  name: string;
  phoneNumber: Number;
  email: string;
  password: string;
  agent: string;
  TIN: string;
}

export default function Signup() {
  const [input, setInput] = useState<string>();
  const { register, handleSubmit } = useForm<FormInput>();
  const { updateSuccess, updateMessage } = useResponse();
  const {updateLoading} = useLoader()
  const router = useRouter();
  const submit = async (data: FormInput) => {
    updateLoading(true)
    setInput(JSON.stringify(data));
    let button = document.querySelector("#signup") as HTMLButtonElement;
    disable(button);
   await api.post("/agent/create", data).then((res)=>{
      updateLoading(false)
      console.log(res.data.message)
      updateMessage(res.data.message);
    }).catch((err) => {
    });
    disable(button)
    router.push("/auth/login");
    updateSuccess();
  };

  return (
    <div>
      <div className="flex">
        <img
          src="/icons/corner.svg"
          alt="corner"
          className="mt-0 3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden left-0"
        />
        <img
          src="/icons/logo.svg"
          alt="logo"
          className="ml-auto mr-auto mt-4"
        />
      </div>
      <div className="flex  justify-center">
        <div className="mt-4">
          <Image
            width={720}
            height={600}
            alt="signup image"
            src={"/images/signup.png"}
            className="mt-28 3xl:w-[35vw] 2xl:w-[40vw] xl:w-[45vw] lg:w-[50vw] md:w-0 sm:w-0"
          />
        </div>
        {/* </div> */}
        <div className="float-right 3xl:w-1/2 2xl:w-1/2 xl:w-1/2 lg:w-1/2 md:w-full sm:w-full mt-8   h-auto 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20">
          <h1 className="text-center font-karla font-bold text-2xl drop-shadow-lg ">
            Sign up
          </h1>
          <form
            action=""
            onSubmit={handleSubmit(submit)}
            className="block w-3/4 mt-8 3xl:ml-[15%] 2xl:ml-[15%] xl:ml-[15%] lg:ml-[15%] md:ml-[12%] sm:ml-[12%]"
          >
            <input
              {...register("name")}
              type="text"
              name="name"
              placeholder="Name"
              required
              className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-11/12 rounded-lg pl-8 font-inter"
            />
            <input
              {...register("phoneNumber")}
              name="phoneNumber"
              type="telephone"
              placeholder="Phone number"
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-11/12 rounded-lg pl-8 mt-4 f font-inter"
              required
            />
            <input
              {...register("email")}
              name="email"
              type="email"
              placeholder="Email"
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-11/12  rounded-lg pl-8 mt-4  font-inter"
              required
            />
                    <input
              {...register("TIN")}
              name="TIN"
              type="text"
              placeholder="TIN or NID"
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-11/12  rounded-lg pl-8 mt-4 font-inter"
              required
            /> 
            <input
              {...register("password")}
              type="password"
              placeholder="Password"
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-11/12  rounded-lg pl-8 mt-4 font-inter"
              required
            />

            {/* <select
              {...register("agent")}
              name="agent"
              id=""
              required
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14  w-5/6  rounded-lg pl-8 mt-6 font-inter"
            >
              <option value="" className="font-inter">Type of agent</option>
              <option value="admin" className="font-inter">Admin</option>
              <option value="superReseller" className="font-inter">Super Reseller</option>
              <option value="reseller" className="font-inter">Reseller</option>
             <option value="agent" className="font-inter">Agent</option>
            </select> */}
            <input
              name="checkbox"
              type="checkbox"
              required
              className="h-4 w-4 mt-4 border-solid border border-[#6C63FF] border-opacity-10 font-inter"
            />
            <span className="pl-2 font-karla">
              I agree to the terms and conditions.
            </span>
            <button
              type="submit"
              id = "signup"
              className="rounded-lg bg-[#6C63FF] block text-white w-52 h-12 ml-auto mr-auto mt-6 font-semibold font-inter"
            >
              <SpinnerLoader/>
              Sign up
            </button>
            <div className="flex mt-4 justify-center">
              {/* <img src="/icons/line1.svg" alt="line1" /> */}
              <div className="h-[1px] opacity-20 w-40 bg-[#6C63FF] mt-3 font-inter"></div>
              <p className="text-[#6C63FF] pl-2 pr-2 font-inter">OR</p>
              <div className="h-[1px] opacity-20 w-40 bg-[#6C63FF] mt-3"></div>
              {/* <img src="/icons/line1.svg" alt="line1" /> */}
            </div>
            {/* <Link href="agentCreate">
              <button className="bg-[#D9D9D9] bg-opacity-10 flex shadow-md font-karia  shadow-slate-400  rounded-lg h-12 w-64 text-center items-center pl-8 ml-auto mr-auto mt-4 font-inter">
                <img src="/icons/google.svg" alt="google" className="pr-3" />
                Sign In With Google
              </button>
            </Link> */}
            <p className="font-karia pt-8 text-center font-inter">
              Have an account?
              <Link
                href="/auth/login"
                className="text-[#6C63FF] pl-1 font-inter"
              >
                Log in
              </Link>
            </p>
          </form>
          {/* <div className="">
            <img
              src="/icons/corner2.svg"
              alt="corner"
              className=" absolute right-0"
            />
          </div> */}
        </div>
      </div>
    </div>
  );
}
