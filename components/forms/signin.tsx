import Image from "next/image";
import Link from "next/link";
import { useForm } from "react-hook-form";
import { useState, useEffect, useReducer } from "react";
import { useCookies } from "react-cookie";
import useAuth from "../context/authContext";
import SpinnerLoader from "../spinnerLoader/spinnerLoader";
import { XCircle, Upload, Router } from "react-bootstrap-icons";
import React from "react";
import { api } from "../utils/api";
import { getCookie } from "cookies-next";
import { disable } from "../utils/disableButton";

interface LoginInput {
  email: string;
  password: string;
  confirmPassword: string;
  OTP: string;
}

type ACTIONTYPE =
  | { type: "addEmail" }
  | { type: "addPassword" }
  | { type: "verifyEmail" };

const initialEvent = {
  addEmail: true,
  addPassword: false,
  verifyEmail: false,
};
const handleEvent = (state: typeof initialEvent, action: ACTIONTYPE) => {
  state = {
    addEmail: false,
    addPassword: false,
    verifyEmail: false,
  };
  switch (action.type) {
    case "addEmail":
      return {
        ...state,
        addEmail: true,
      };
    case "addPassword":
      return {
        ...state,
        addPassword: true,
      };
    case "verifyEmail":
      return {
        ...state,
        verifyEmail: true,
      };
    default:
      return {
        ...state,
      };
  }
};

export default function Signin() {
  const [fields, setFields] = useState<string>();
  const { reset, register, handleSubmit } = useForm<LoginInput>();
  const { user, login } = useAuth();
  const [forgotPassword, setForgotPassword] = useState<Boolean>(false);
  const [state, dispatch] = useReducer(handleEvent, initialEvent);
  const onSubmit = async (values: LoginInput) => {
    let mybutton = document.querySelector("#button") as HTMLButtonElement;
    disable(mybutton);
    login(values, mybutton);
  };
  const forgotPasswordHandler = async (data: LoginInput) => {
    setFields(JSON.stringify(data));
    const url = "/agent/forgotPassword";
    await api.post(url, data).then((res) => {
      dispatch({type: "verifyEmail"})
    });
  };
  const emailVerification = async (data: LoginInput) => {
    setFields(JSON.stringify(data));
    const url = "/agent/verify-email";
    await api.post(url, data).then((res) => {
      dispatch({type: "addPassword"})
    });
  };
  const resetPasswordHandler = async (data: LoginInput) => {
    setFields(JSON.stringify(data));
    const url = "agent/reset-password";
    await api.post(url, data).then((res) => {
      setForgotPassword(false);
    });
  };

  return (
    <div>
      <div className="flex">
        <img
          src="/icons/corner1.svg"
          alt="corner"
          className="3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden left-0 "
        />
        <img
          src="/icons/logo.svg"
          alt="logo"
          className="ml-auto mr-auto mt-4"
        />
        <img src="/icons/dots.svg" alt="dots" className="3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden right-0" />
      </div>

      <div className="flex mt-8 justify-center">
        <div className="mt-6">
          <Image
            width={600}
            height={600}
            alt="sign in image"
            src={"/images/login.png"}
            className=" mt-28 3xl:w-[30vw] 2xl:w-[40vw] xl:w-[50vw] lg:w-[50vw] md:w-0 sm:w-0 "
          />
        </div>
        <div className="float-right 3xl:w-1/2 2xl:w-1/2 xl:w-1/2 lg:w-1/2 md:w-full sm:w-full  h-auto 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-20">
          <h1 className="text-center font-karla font-bold text-3xl drop-shadow-lg pt-8">
            Welcome back
          </h1>
          <form
            action=""
            onSubmit={handleSubmit(onSubmit)}
            className="block w-3/4 mt-16 3xl:ml-[15%] 2xl:ml-[15%] xl:ml-[15%] lg:ml-[15%] md:ml-[10%] sm:ml-[10%]"
          >
            <input
              {...register("email")}
              type="email"
              placeholder="Email"
              className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-11/12 rounded-lg pl-8 font-inter"
            />
            <div>
            <input
              {...register("password")}
              type="password"
              placeholder="Password"
              className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-11/12  rounded-lg pl-8 mt-4 font-inter"
            />
            </div>
            <div className="flex justify-between mt-4">
              <div className="flex ">
                <input
                  type="checkbox"
                  className="h-4 w-4 mt-4 relative  border-solid border border-[#6C63FF] border-opacity-10 flex"
                />
                <span className="pl-2 font-inter pt-3">Remember me</span>
              </div>

              <button
                type="button"
                onClick={() => {
                  setForgotPassword(true);
                }}
                id="button"
                className="pt-3 font-inter text-[#6C63FF] login 3xl:mr-16 2xl:mr-16 xl:mr-16 lg:mr-16 md:mr-8 sm:mr-8"
              >
                Forgot password?
              </button>
            </div>
            <button
              type="submit"
              className="rounded-lg bg-[#6C63FF] block text-white w-52 h-12 ml-auto mr-auto mt-10 font-semibold font-inter hover:opacity-75"
            >
               <SpinnerLoader />
              Log in
            </button>
            <div className="flex mt-4 justify-center">
              <div className="h-[1px] opacity-20 w-40 bg-[#6C63FF] mt-3"></div>
              <p className="text-[#6C63FF] pl-2 pr-2  font-inter">OR</p>
              <div className="h-[1px] opacity-20 w-40 bg-[#6C63FF] mt-3"></div>
            </div>
            {/* <button className="bg-[#D9D9D9] bg-opacity-10 flex shadow-md font-karia  shadow-slate-400  rounded-lg h-12 w-64 text-center items-center pl-8 ml-auto mr-auto mt-4 font-inter">
              <img src="/icons/google.svg" alt="google" className="pr-3" />
              Sign In With Google
            </button> */}
            <p className="font-inter pt-8 text-center">
              Don't have an account?
              <Link
                href="/auth/signup"
                className="text-[#6C63FF] pl-1 font-inter"
              >
                Sign up
              </Link>
            </p>
          </form>
          <div>
            {forgotPassword && (
              <div className="h-full w-full absolute top-0 bg-gray-800 opacity-90 left-0">
                {state.addEmail && (
          <div className="3xl:h-[40vh] 2xl:h-[40vh] xl:h-[40vh] lg:h-[40vh] md:h-auto sm:h-auto md:pb-8 sm:pb-8  3xl:w-1/3 2xl:w-1/3 xl:w-2/3 lg:w-1/2 md:w-3/4 sm:w-5/6 bg-white shadow-sm shadow-slate-400 ml-auto mr-auto mt-24">
          <XCircle
                      className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
                      onClick={() => setForgotPassword(false)}
                    />
                    <h1 className="text-2xl font-bold font-karla text-center pt-6">
                      Forgot Password?
                    </h1>
                    <p className=" text-center font-karla pl-20 pr-20 pt-6 pb-6">
                      Please enter the email associated with your account and
                      we’ll send an email with link, where you can change your
                      password
                    </p>
                    <form
                      action=""
                      onSubmit={handleSubmit(forgotPasswordHandler)}
                    >
                      <label htmlFor="" className="font-karla pl-20">
                        Email
                      </label>
                      <input
                        {...register("email")}
                        type="email"
                        placeholder="Email"
                        className=" block border-solid border border-[#6C63FF] border-opacity-50 h-14  w-3/4 rounded-lg pl-8 font-inter 3xl:ml-20 2xl:ml-20 xl:ml-20 lg:ml-14 md:ml-8 sm:ml-8 mt-3"
                      />
                      <div className="flex justify-center">
                        <button
                          type="submit"
                          className="h-14 w-40 text-white bg-[#6C63FF] rounded-lg font-karla mt-4"
                        >
                          Send
                        </button>
                      </div>
                    </form>
                  </div>
                )}
              </div>
            )}
            {state.verifyEmail && (
              <div className="container mx-auto">
                <div className="max-w-sm mx-auto md:max-w-lg">
                  <div className="w-full">
                    <div className="bg-white h-64 py-3 rounded text-center">
                      <h1 className="text-2xl font-bold">OTP Verification</h1>
                      <div className="flex flex-col mt-4">
                        <span>Enter the OTP you received at</span>
                        <span className="font-bold">
                          example.....@gmail.com
                        </span>
                      </div>
                     <form onSubmit={handleSubmit(emailVerification)}>
                      <div
                        id="otp"
                        className="flex flex-row justify-center text-center px-2 mt-5"
                      >
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="first"
                          max-length="1"
                        />
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="second"
                          max-length="1"
                        />
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="third"
                          max-length="1"
                        />
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="fourth"
                          max-length="1"
                        />
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="fifth"
                          max-length="1"
                        />
                        <input
                          className="m-2 border h-10 w-10 text-center form-control rounded"
                          type="text"
                          id="sixth"
                          max-length="1"
                        />
                      </div>

                      <div className="flex justify-center text-center mt-5">
                        <a className="flex items-center text-blue-700 hover:text-blue-900 cursor-pointer">
                          <span className="font-bold">Resend OTP</span>
                          <i className="bx bx-caret-right ml-1"></i>
                        </a>
                        <button type="submit" className="h-8 w-28 bg-white border-2 border-blue-500 border-solid text-[#6C63FF]">Verify</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {state.addPassword && (
              <div>
                <XCircle
                  className="float-right text-xl hover:text-[#6C63FF] hover:text-2xl mt-2 mr-2 text-[#6C63FF]"
                  onClick={() => setForgotPassword(false)}
                />
                <h1 className="text-2xl font-bold font-karla text-center pt-6">
                  Reset Password
                </h1>
                <form action="" onSubmit={handleSubmit(resetPasswordHandler)}>
                  <label htmlFor="" className="font-karla pl-20">
                    New Password
                  </label>
                  <input
                    type="password"
                    {...register("password")}
                    placeholder="New password"
                    className=" block border-solid border border-[#6C63FF] border-opacity-50 h-14  w-3/4 rounded-lg pl-8 font-inter ml-20 mt-3"
                  />
                  <label htmlFor="" className="font-karla pl-20">
                    Confirm Password
                  </label>
                  <input
                    {...register("confirmPassword")}
                    type="password"
                    placeholder="Confirm Password"
                    className=" block border-solid border border-[#6C63FF] border-opacity-50 h-14  w-3/4 rounded-lg pl-8 font-inter ml-20 mt-3"
                  />
                  <div className="flex justify-center">
                    <button
                      type="submit"
                      className="h-14 w-40 text-white bg-[#6C63FF] rounded-lg font-karla mt-4"
                    >
                      Save
                    </button>
                  </div>
                  <p className="font-karla text-center">
                    Go back to
                    <span className="text-[#6C63FF]">
                      <a href="/">Log in</a>
                    </span>
                  </p>
                </form>
              </div>
            )}
            <div className="3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden left-0 bottom-0">
              <img src="/icons/dots.svg" alt="dots" />
            </div>
            <div className="3xl:absolute 2xl:absolute xl:absolute lg:absolute md:hidden sm:hidden right-0 bottom-0 ">
              <img src="/icons/corners3.svg" alt="corner" className="mt-0  " />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
