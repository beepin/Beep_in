import React, { useState } from 'react';
import { useReducer } from 'react';
import { useForm } from 'react-hook-form';
import { api } from '../utils/api';
import useResponse from '../context/responseContext';
interface FormInput {
  name?: string;
  email?: string;
  phoneNumber?: string;
  password?: string;
  position?: number;
  TIN? : string;
  buying_price?: number;
}
type ACTIONTYPE =
  | { type: 'agentType' }
  | { type: 'superReseller' }
  | { type: 'reseller' }
  | { type: 'agent' };

const initialUser = {
  agentType: true,
  superReseller: false,
  reseller: false,
  agent: false,
};
const createUser = (user: typeof initialUser, action: ACTIONTYPE) => {
  user = {
    agentType: false,
    superReseller: false,
    reseller: false,
    agent: false,
  };
  switch (action.type) {
    case 'agentType':
      return {
        ...user,
        agentType: true,
      };
    case 'superReseller':
      return {
        ...user,
        superReseller: true,
      };
    case 'reseller':
      return {
        ...user,
        reseller: true,
      };
    case 'agent':
      return {
        ...user,
        agent: true,
      };
    default:
      return {
        ...user,
      };
  }
};

function CreateAgent() {
  const [ dispatch] = useReducer(createUser, initialUser);
  const { register, handleSubmit } = useForm<FormInput>();
  const { updateSuccess, updateMessage } = useResponse();
  const [agentType, updateType] = useReducer(
    (prev: any, next: any) => {
      return { ...prev, ...next };
    },
    { type: '', position: 0 },
  );

  const create = (data: FormInput) => {
    api.post('/agent/register', data).then((res: any) => {
      updateSuccess();
      updateMessage(res.data.message);
    });
  };
  return (
    <div className='w-full'>
      {/* {user.agentType && (
        <div className="flex flex-col mt-24">
          <h1 className="text-3xl font-extrabold text-center uppercase">
            Which agent do you want to create?
          </h1>
          <div className="mt-14 flex flex-col ml-auto mr-auto">
            <button
              onClick={() => {
                dispatch({ type: 'agent' });
                updateType({ type: 'Reseller', position: 4 });
              }}
              className="h-16 w-96 bg-[#6C63FF] text-white text-center rounded-xl mt-10"
            >
              RESELLER
            </button>
            <button
              onClick={() => {
                dispatch({ type: 'agent' });
                updateType({ type: 'Agent', position: 3 });
              }}
              className="h-16 w-96 bg-[#6C63FF] text-white text-center rounded-xl mt-10"
            >
              AGENT
            </button>
          </div>
        </div>
      )} */}

        <div className='w-full mt-16'>
     
          <p className="text-center font-karla text-xl drop-shadow-md shadow-black">
            Register Agents to give them access to Beep-In platform
          </p>
          <div className="h-96 3xl:w-1/2 2xl:w-1/2 xl:w-1/2 lg:w-2/3 md:w-2/3 sm:w-3/4 ml-auto mr-auto mt-8">
            <form
              action=""
              className="flex flex-col"
              onSubmit={handleSubmit(create)}
            >
             <div className="flex w-full mt-4">
              <input
                {...register('name')}
                name="name"
                type="text"
                placeholder="Names"
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              </div>
              <div className="flex w-full mt-4">

              <input
                {...register('email')}
                name="email"
                type="text"
                placeholder="Email"
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              </div>
              <div className="flex w-full mt-4">
              <input
                {...register('phoneNumber')}
                name="phoneNumber"
                type="number"
                placeholder="Contact"
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              </div>
              <div className="flex w-full mt-4">
              <input
                type="text"
                {...register('TIN')}
                name="TIN"
                placeholder="TIN/NID"
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              </div>
              <div className="flex w-full mt-4">
              <input
                {...register('position')}
                name="position"
                type="text"
                placeholder="Address"
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              <input
                {...register('buying_price')}
                name="buying_price"
                type="number"
                placeholder="Buying price"
                className="block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                required
              />
              </div>
              <div className="flex w-full mt-4">

              <select 
                {...register('position')}
                name="position"
                placeholder='Group name'
                className=" block border-solid border border-[#6C63FF] border-opacity-10 h-14 w-full rounded-lg pl-8  "
                >
                <option value="">Privilege</option>
                <option value={3}>Reseller</option>
                <option value={4}>Agent</option>
                
              </select>
              </div>
              <div className="mt-8 flex justify-between">
                <button
                  type="button"
                  onClick={handleSubmit(create)}
                  className="h-14 w-28 bg-[#6C63FF] text-white rounded-xl "
                >
                  SAVE
                </button>
              </div>
            </form>
          </div>
        </div>
    </div>
  );
}

export default CreateAgent;
