export function downloadCSV(csv : any, filename:any) {
    var csvFile;
    var downloadLink;
   
    //define the file type to text/csv
    csvFile = new Blob([csv], {type: 'text/csv'});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";

    document.body.appendChild(downloadLink);
    downloadLink.click();
}

export function exportTableToCSV(data:string[], filename:string) {
   var csv = [];
	csv.push(data.join(","));
   downloadCSV(csv.join("\n"), filename);
}

