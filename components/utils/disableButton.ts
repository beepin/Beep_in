export function disable(button : HTMLButtonElement  ){
    button.disabled = !button.disabled;
}