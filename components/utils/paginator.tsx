import React, { useEffect, useReducer } from 'react';
import { ChevronLeft, ChevronRight } from 'react-bootstrap-icons';

function Paginator({ numPages, data, setCurrent, current }:any) {

  const pages = [];
  for (let i = 0; i < numPages; i++) {
    pages.push(i + 1);
  }
  const [positions, setPositions] = useReducer(
    (prev:any, next:any) => {
      return { ...prev, ...next };
    },
    { start: 0, end: 10 },
  );
  const updateCurrent = () => {
    if (positions.start +  5 > data.length) {
      setPositions({ start: 0, end: 5 });
      setCurrent(data.slice(positions.start, positions.end));
    } else {
      setPositions({ start: positions.start + 5, end: positions.end + 5 });
      setCurrent(data.slice(positions.start, positions.end));
    }
  };
  const back = () => {
    if (positions.start - 5 >= 0 ) {
      setPositions({ start: positions.start - 5, end: positions.end - 5 });
      setCurrent(data.slice(positions.start, positions.end));
    }
  };
  const num = (page : number)=>{
    if((page *  10) - 10 <= data.length ){
      setPositions({start: (page* 10) - 10, end: page * 10})
    }
    setCurrent(data.slice(positions.start, positions.end));
  }
  return (
 
  <div className='flex justify-center mt-2'>

      {pages.length > 5 && (
        <button className='bg-gray-100 rounded-full h-8 w-8 text-center mt-2 hover:bg-blue-500 hover:text-whiteactive:bg-blue-500 focus:outline-none focus:bg-blue-500 focus:border-none focus:text-white' onClick={back}>
           <ChevronLeft className="ml-2" />
        </button>
      )}
      <div className="flex gap-2 p-2">

      {pages.map((page, index) => (
        <>{index < 5 && <button key={index} onClick={()=>{num(page)}} className="bg-white border-[1px] hover:bg-blue-500 hover:text-white hover:border-none border-gray-100 text-gray-600 w-8 h-8 rounded-full active:bg-blue-500 focus:outline-none focus:bg-blue-500 focus:border-none focus:text-white">{page}</button>}</>
        ))}
        </div>
      {pages.length > 5 && (
        <button className='bg-gray-100 hover:bg-blue-500 hover:text-white rounded-full h-8 w-8 text-center mt-2 active:bg-blue-500 focus:outline-none focus:bg-blue-500 focus:border-none focus:text-white' onClick={updateCurrent}>
        <ChevronRight className="ml-2" />
        </button>
      )}
 </div>
  );
}

export default Paginator;
