import axios, { AxiosError } from "axios";
import { getCookie } from "cookies-next";

const baseURL = process.env.NEXT_PUBLIC_BEEP_IN_BACKEND_URL!;

axios.interceptors.response.use(
    response => response,
    error => {
        if(error.response){    
         if(error.response.status === 404){
            console.log( "axios error", 'Resource not found.');
        }else if(error.response.status === 401 || error.response.status == 403){
            console.log("axios error",'Unauthorized access!');
        }else{
            console.log("500 error",'An error has occured!');
        }
    }else if(error.request){
        console.log("axios error",'Request error!');
    }
    return Promise.reject(error);
}
)
export const api = axios.create({
    baseURL: baseURL,
    headers: {
        'Content-Type': 'application/json',
        'authorization': "Bearer " + getCookie("accessToken")
    },
    // withCredentials: true, // to send cookie
})
