import '../styles/globals.css';
import type { AppProps } from 'next/app';
import useAuth, { AuthProvider } from '../components/context/authContext';
import { ResponseProvider } from '../components/context/responseContext';
import { useEffect } from 'react';
import { getCookie } from 'cookies-next';
import { LoaderProvider } from '../components/context/loaderContext';
export default function App({ Component, pageProps }: AppProps) {
  const { updateUser, user, priv } = useAuth();
  useEffect(() => {
    updateUser();
  }, []);
  return (
    <LoaderProvider>
      <ResponseProvider>
        <AuthProvider>
          <Component {...pageProps} />
        </AuthProvider>
      </ResponseProvider>
    </LoaderProvider>
  );
}
