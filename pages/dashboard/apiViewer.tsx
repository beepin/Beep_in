import React from 'react'
import LeftSidebar from '../../components/dashboard/leftSidebar';
import TopNavbar from '../../components/dashboard/topBar';
import ViewApi from '../../components/dashboard/viewApi';

function ApiViewer() {
  return (
    <div className = "h-screen overflow-y-hidden">
        <TopNavbar />
        <div className='flex'>
        <LeftSidebar />
       <ViewApi/>
       </div>
    </div>
  )
}

export default ApiViewer;