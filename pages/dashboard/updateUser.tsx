import React from 'react'
import LeftSidebar from '../../components/dashboard/leftSidebar'
import TopNavbar from '../../components/dashboard/topBar'
import UpdateForms from '../../components/forms/updateForms'
import Response from '../../components/utils/response'

function UpdateUser() {
  return (
    <div>
        <div className="h-screen overflow-y-hidden">
        <TopNavbar />
        <div className='flex'>
        <LeftSidebar />
        <UpdateForms />
        </div>
        <Response/>
    </div>
    </div>
  )
}

export default UpdateUser