import React from 'react';
import LeftSidebar from '../../components/dashboard/leftSidebar';
import TopNavbar from '../../components/dashboard/topBar';
import CreateAgent from '../../components/forms/createAgent';
import Response from '../../components/utils/response';
function AgentCreate() {
  return (
    <div className="h-screen w-full overflow-y-hidden">
      <TopNavbar />
      <div className="flex w-full">
        <LeftSidebar />
        <CreateAgent />
      </div>
      <Response />
    </div>
    //   <div>
    //  <CreateAgent />
    //   </div>
  );
}

export default AgentCreate;
