import React from 'react'
import Signin from '../../components/forms/signin'
import Response from '../../components/utils/response'


const login = () => {
  return (
    <div>
        <Signin/>
        <Response/>
    </div>
  )
}

export default login