import React from 'react'
import Signup from '../../components/forms/signup'
import Response from '../../components/utils/response'

const signup = () => {
  return (
    <div>
        <Signup/>
        <Response/>
    </div>
  )
}

export default signup