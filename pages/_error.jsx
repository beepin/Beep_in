function Error({statusCode}){
    return (
        <>
        <div className="h-auto w-1/2  bg-white border-[1px] border-blue-200 pt-10 pb-10 rounded-xl ml-auto mr-auto mt-[25%]">
        <img
            src="/icons/logo.svg"
            alt="logo"
            className="ml-auto mr-auto" />
            <p className="text-2xl text-center pt-6 font-karla text-red-600">{statusCode
                ? `An error ${statusCode} occurred on server`
                : 'An error occurred on client'}
               
                </p>
                </div>
                </>
    )
}
Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    return { statusCode }
}   
export default Error;