import styles from '../styles/Home.module.css'

import HomePage from '../components/landing/homePage'
import useAuth, { AuthProvider } from '../components/context/authContext'
import { useEffect } from 'react'

export default function Home() {

  return (
    <AuthProvider>
   <div className={styles.container}>
      <HomePage />
   </div>
   </AuthProvider>
  )
}
